#The MainTrack project

Simple planner of your affairs

##Functionality

     ● Users         : $ maintrack user     : Adding, Viewing, Logging      
     ● Tags          : $ maintrack tag      : Adding, Editing, Viewing, Deleting
     ● Tasks         : $ maintrack task     : Adding, Adding subtasks, Editing (e.g. rights), Viewing, Deleting
     ● Regular tasks : $ maintrack reg_task : Adding, Editing, Viewing, Deleting
     ● Events        : $ maintrack user     : Adding, Editing, Viewing

##Installing 

####Cloning :
```bash
$ git clone https://bitbucket.org/TanyaGlebova/python.git
```
####Installing setuptools :
```bash
$ pip3 install -U pip setuptools 
```
####Installing library :
```bash
$ cd library
$ python3 setup.py install
```
####Installing console :
```bash
$ cd console
$ python3 setup.py install
```
####Unittests :
```bash
$ python3 setup.py test
```
##Example of using
```bash
$ maintrack task add -t "Cook a salad"
$ maintrack user add woman
$ maintrack task rights add read -tid 106 -uid 6
$ maintrack task show can_read
```

     further instructions you can get with using option -h or --help