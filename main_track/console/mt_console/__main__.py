import os
from .arg_parser import handle_commands
#import console.mt_console.config as config
from . import config as config
from mt_library.logger import setup_library_logging


def main():
    _create_app_data_folder()
    _setup_lib_logging()
    handle_commands()


def _create_app_data_folder():
    if not os.path.exists(config.APP_DATA_DIRECTORY):
        os.makedirs(config.APP_DATA_DIRECTORY)


def _setup_lib_logging():
    log_file_path = os.path.join(config.LOGS_DIRECTORY, config.LOG_FILE)
    setup_library_logging(
        enabled=config.LOGIN_ENABLED,
        log_all_levels=config.LOG_ALL_LEVELS,
        log_file_path=log_file_path,
        log_format=config.LOG_FORMAT)


if __name__ == '__main__':
    main()
