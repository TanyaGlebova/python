import sys
import argparse
import datetime
import dateparser
import humanize
#from console.mt_console.user import UserDatabase, UserObject as User
#from console.mt_console.user_session import UserSession
#import console.mt_console.config as config
from . import config as config
from .user_session import UserSession
from .user import UserDatabase, UserObject as User
from mt_library import commands
import mt_library.controllers.tags_controller
import mt_library.controllers.events_controller
import mt_library.controllers.tasks_controller
import mt_library.controllers.reg_tasks_controller
from mt_library.entities.tag import Tag
from mt_library.entities.event import Event
from mt_library.entities.task import Task, TaskStatus, TaskPriority
from mt_library.entities.reg_task import RegTask
from mt_library.exceptions.exceptions import UserHasNoRightError


class DefaultHelpParser(argparse.ArgumentParser):
    def error(self, message):
        print('error: %s\n' % message, file=sys.stderr)
        self.print_help()
        sys.exit(2)


MIN_INTERVAL = 300  # minimum interval for repeating regular tasks


def init_parser():
    parser = DefaultHelpParser(
        description='Console task-manager Main Track',
        usage='''Main Track <object> [<args>]''')
    subparser = parser.add_subparsers(
        dest='object',
        title='Commands',
        description='Choose object you want to work with',
        metavar='')

    user = subparser.add_parser(
        'user',
        help='Manage users',
        usage='Main Track user')

    user_parser = user.add_subparsers(
        dest='action',
        title='Manage users',
        description='Commands to work with users',
        metavar='')

    create_user_parser(user_parser)

    tag = subparser.add_parser(
        'tag',
        help='Manage tags',
        usage='Main Track tag')

    tag_parser = tag.add_subparsers(
        dest='action',
        title='Manage tags',
        description='Commands to work wih tags',
        metavar='')

    create_tag_parser(tag_parser)

    task = subparser.add_parser(
        'task',
        help='Manage tasks',
        usage='Main Track task')

    task_parser = task.add_subparsers(
        dest='action',
        title='Manage task',
        description='Commands to work with task',
        metavar='')

    create_task_parser(task_parser)

    reg_task = subparser.add_parser(
        'reg',
        help='Manage regular tasks',
        usage='Main Track reg')

    reg_task_parser = reg_task.add_subparsers(
        dest='action',
        title='Manage regular tasks',
        description='Commands to work with regular tasks',
        metavar='')

    create_reg_task_parser(reg_task_parser)

    event = subparser.add_parser(
        'event',
        help='Manage events',
        usage='Main Track event')

    event_parser = event.add_subparsers(
        dest='action',
        title='Manage events',
        description='Commands to work with events',
        metavar='')

    create_event_parser(event_parser)

    return parser


def create_user_parser(parser):
    parser.add_parser('add', help='Add new user').add_argument('username')
    parser.add_parser(
        'login', help='Authorize user dy username').add_argument('username')
    parser.add_parser('logout', help='Logout user')
    parser.add_parser('current', help='Show current user')
    parser.add_parser('all', help='Show all users')


def create_tag_parser(parser):
    parser.add_parser('add', help='Add new tag').add_argument(
        'name', help="tag's name")
    parser.add_parser('show', help='Show tag by id').add_argument(
        'id', help="tag's id")
    update_tag_parser = parser.add_parser('edit', help='Edit tag by id')
    update_tag_parser.add_argument(
        '-id', '--id', help="Tag's id", required=True)
    update_tag_parser.add_argument(
        '-n', '--name', help="Tag's name", required=True)
    parser.add_parser('delete', help="Delete tag by id").add_argument(
        'id', help="Tag's id")
    parser.add_parser('all', help='Show all tags')


def create_task_parser(parser):
    add_task_parser = parser.add_parser('add', help='Add new task')
    add_task_required_arguments = add_task_parser.add_argument_group(
        'required arguments')
    add_task_required_arguments.add_argument(
        '-t', '--title', help="Task's title", required=True)
    add_task_optional_arguments = add_task_parser.add_argument_group(
        'optional arguments')
    add_task_optional_arguments.add_argument(
        '-n', '--note', help="Task's note")
    add_task_optional_arguments.add_argument(
        '-b', '--begin', help="Task's begin")
    add_task_optional_arguments.add_argument('-e', '--end', help="Task's end")
    add_task_optional_arguments.add_argument(
        '-ti', '--tag_id', help="Task's tag id")
    add_task_optional_arguments.add_argument('-p', '--priority', choices=TaskPriority.__members__,
                                             help="Task's priority")
    add_task_optional_arguments.add_argument(
        '-r', '--regular', help='Create regular task according to interval')
    add_task_optional_arguments.add_argument(
        '-sd', '--start_date', help='Start date time for regular task')
    add_task_optional_arguments.add_argument(
        '-ev', '--event', help='Create event task')
    add_task_optional_arguments.add_argument(
        '-pl', '--place', help='Place for event')

    edit_task_parser = parser.add_parser('edit', help='Edit task')
    edit_task_required_arguments = edit_task_parser.add_argument_group(
        'required arguments')
    edit_task_required_arguments.add_argument('-id', '--id', help="Task's id")
    edit_task_optional_arguments = edit_task_parser.add_argument_group(
        'optional arguments')
    edit_task_optional_arguments.add_argument(
        '-t', '--title', help="Task's title")
    edit_task_optional_arguments.add_argument(
        '-n', '--note', help="Task's note")
    edit_task_optional_arguments.add_argument(
        '-b', '--begin', help="Task's begin")
    edit_task_optional_arguments.add_argument('-e', '--end', help="Task's end")
    edit_task_optional_arguments.add_argument(
        '-ti', '--tag_id', help="Task's tag id")
    edit_task_optional_arguments.add_argument('-p', '--priority', choices=TaskPriority.__members__,
                                              help="Task's priority")

    show_task_parser = parser.add_parser('show', help='Show tasks')
    show_task_sub_parser = show_task_parser.add_subparsers(dest='show_action', title='Show tasks',
                                                           description='Commands to show tasks', metavar='')
    create_show_task_parser(show_task_sub_parser)

    parser.add_parser('delete', help='Delete task by id').add_argument(
        'id', help="Task's id")
    set_status_parser = parser.add_parser(
        'set_status', help='Set status to task')
    set_status_sub_parser = set_status_parser.add_subparsers(dest='set_status_action', title='Set status',
                                                             description='Set status to task', metavar='')
    set_status_sub_parser.add_parser(
        'new', help='Set task as NEW by id').add_argument('id', help="Task's id")
    set_status_sub_parser.add_parser('in_work', help='Set task as IN WORK by id').add_argument('id',
                                                                                               help="Task's id")
    set_status_sub_parser.add_parser(
        'done', help='Set task as DONE by id').add_argument('id', help="Task's id")

    create_sub_task_parser = parser.add_parser(
        'add_sub', help="Add sub task by parent task's id")
    create_sub_task_required_arguments = create_sub_task_parser.add_argument_group(
        'required arguments')
    create_sub_task_required_arguments.add_argument(
        '-pd', '--parent_task_id', help="Parent task's id", required=True)
    create_sub_task_required_arguments.add_argument(
        '-t', '--title', help="Task's title", required=True)
    create_sub_task_optional_arguments = create_sub_task_parser.add_argument_group(
        'optional arguments')
    create_sub_task_optional_arguments.add_argument(
        '-n', '--note', help="Task's note")
    create_sub_task_optional_arguments.add_argument(
        '-b', '--begin', help="Task's begin")
    create_sub_task_optional_arguments.add_argument(
        '-e', '--end', help="Task's end")
    create_sub_task_optional_arguments.add_argument(
        '-ti', '--tag_id', help="Task's tag id")
    create_sub_task_optional_arguments.add_argument('-p', '--priority', choices=TaskPriority.__members__,
                                                    help="Task's priority")
    assign_task_parser = parser.add_parser('assign', help="Assigns task on user")
    assign_task_parser.add_argument('-tid', '--task_id', help="assigned task's ID", required=True)
    assign_task_parser.add_argument('-uid', '--user_id', help="user's ID", required=True)

    rights_parser = parser.add_parser('rights', help="Manage user's rights")
    rights_subparser = rights_parser.add_subparsers(dest='rights_action', title='Manages rights',
                                                    description='Commands to manage rights', metavar='')
    create_rights_parser(rights_subparser)


def create_show_task_parser(parser):
    parser.add_parser(
        'id',
        help='Show task by id',
        usage='Main Track task show id').add_argument('id', help="Task's id")
    parser.add_parser('all', help="Show all user's tasks")

    show_sub_tasks_parser = parser.add_parser(
        'sub', help="Show sub tasks by parent task's id")
    show_sub_tasks_parser.add_argument('pd', help="Parent task's id")

    parser.add_parser(
        'parent',
        help="Show parent task by sub task's id").add_argument('id', help="Sub task's id")

    parser.add_parser('assigned', help="Show tasks assigned on current user")
    parser.add_parser('new', help="Show user's NEW task's")
    parser.add_parser('in_work', help="Show user's IN WORK task's")
    parser.add_parser('done', help="Show user's DONE task's")

    parser.add_parser('can_read', help="Show tasks that current user can read")
    parser.add_parser(
        'can_write', help="Show tasks that current user can read and write")


def create_reg_task_parser(parser):
    edit_reg_task_parser = parser.add_parser('edit', help="edit reg task")
    edit_reg_task_parser.add_argument('id', help="Reg's id")
    edit_reg_task_parser.add_argument(
        '-r', '--regular', help="Create regular task according to interval")
    edit_reg_task_parser.add_argument(
        '-sd', '--start_date', help="Start date for regular task")
    parser.add_parser('all', help='Show all regular tasks')


def create_rights_parser(parser):
    add_right_parser = parser.add_parser(
        'add', help='Add right', usage='Main Track task right add')
    add_right_subparser = add_right_parser.add_subparsers(
        dest='rights_add_action',
        title='Add rights',
        description='Commands to add read or write rights',
        metavar='')

    add_user_for_read_parser = add_right_subparser.add_parser(
        'read', help='Give user for read task')
    add_user_for_read_parser.add_argument(
        '-tid', '--task_id', help="Task's id", required=True)
    add_user_for_read_parser.add_argument(
        '-uid', '--user_id', help="User's id", required=True)
    add_user_for_write_parser = add_right_subparser.add_parser(
        'write', help='Give user rights for read and write task')
    add_user_for_write_parser.add_argument(
        '-tid', '--task_id', help="Task's id")
    add_user_for_write_parser.add_argument(
        '-uid', '--user_id', help="User's id")

    remove_right_parser = parser.add_parser(
        'remove', help='Remove right', usage='Main Track task right remove')
    remove_right_subparser = remove_right_parser.add_subparsers(
        dest='rights_remove_action',
        title='Remove rights',
        description='Commands to remove read or write rights',
        metavar='')

    remove_user_for_read_parser = remove_right_subparser.add_parser(
        'read', help='Remove user right for read task')
    remove_user_for_read_parser.add_argument(
        '-tid', '--task_id', help="Task's id", required=True)
    remove_user_for_read_parser.add_argument(
        '-uid', '--user_id', help="User's id", required=True)

    remove_user_for_write_parser = remove_right_subparser.add_parser('write',
                                                                     help='Remove user right for read and write task')
    remove_user_for_write_parser.add_argument(
        '-tid', '--task_id', help="Task's id", required=True)
    remove_user_for_write_parser.add_argument(
        '-uid', '--user_id', help="User's id", required=True)


def create_event_parser(parser):
    edit_event_parser = parser.add_parser('edit', help="edit event")
    edit_event_parser.add_argument('id', help="Event's id")
    edit_event_parser.add_argument('-ev', '--event', help='Create event task')
    edit_event_parser.add_argument('-pl', '--place', help='Place for event')

    parser.add_parser('all', help='Show all events')


def check_user_autorized(user_session):
    if user_session.get_current_user() is None:
        print("You must be logged")
        quit()


def process_object(args, user_session):
    if args.object == 'user':
        process_user_action(args, user_session)
    elif args.object == 'tag':
        check_user_autorized(user_session)
        process_tag_action(args, user_session.get_current_user())
    elif args.object == 'task':
        check_user_autorized(user_session)
        process_task_action(args, user_session.get_current_user())
    elif args.object == 'reg':
        check_user_autorized(user_session)
        process_reg_task_action(args, user_session.get_current_user())
    elif args.object == 'event':
        check_user_autorized(user_session)
        process_event_action(args, user_session.get_current_user())


def process_user_action(args, user_session):
    if args.action == 'login':
        login(args, user_session)
    elif args.action == 'logout':
        logout(user_session)
    elif args.action == 'add':
        add_user(args, user_session)
    elif args.action == 'current':
        check_user_autorized(user_session)
        show_user(user_session.get_current_user())
    elif args.action == 'all':
        show_all_users()


def process_tag_action(args, user):
    if args.action == 'add':
        add_tag(args, user)
    elif args.action == 'edit':
        edit_tag(args, user)
    elif args.action == 'show':
        show_tag(args, user)
    elif args.action == 'delete':
        delete_tag(args, user)
    elif args.action == 'all':
        show_all_tags(user)


def process_task_action(args, user):
    if args.action == 'add':
        add_task(args, user)
    elif args.action == 'edit':
        edit_task(args, user)
    elif args.action == 'show':
        if args.show_action == 'id':
            show_task(args, user)
        elif args.show_action == 'all':
            show_all_tasks(user)
        elif args.show_action == 'sub':
            show_sub_task(args, user)
        elif args.show_action == 'parent':
            show_parent_task(args, user)
        elif args.show_action == 'assigned':
            show_assigned_tasks(user)
        elif args.show_action == 'new':
            show_new_tasks(user)
        elif args.show_action == 'in_work':
            show_in_work_tasks(user)
        elif args.show_action == 'done':
            show_done_tasks(user)
        elif args.show_action == 'can_read':
            show_can_read_tasks(user)
        elif args.show_action == 'can_write':
            show_can_write_tasks(user)
    elif args.action == 'set_status':
        if args.set_status_action == 'new':
            set_task_as_new(args, user)
        elif args.set_status_action == 'in_work':
            set_task_as_in_work(args, user)
        elif args.set_status_action == 'done':
            set_task_as_done(args, user)
    elif args.action == 'delete':
        delete_task(args, user)
    elif args.action == 'add_sub':
        create_sub_task(args, user)
    elif args.action == 'assign':
        assign_task_on_user(args, user)
    elif args.action == 'rights':
        if args.rights_action == 'add':
            if args.rights_add_action == 'read':
                add_user_for_read(args, user)
            elif args.rights_add_action == 'write':
                add_user_for_write(args, user)
        elif args.rights_action == 'remove':
            if args.rights_remove_action == 'read':
                remove_user_for_read(args, user)
            if args.rights_remove_action == 'write':
                remove_user_for_write(args, user)


def process_reg_task_action(args, user):
    if args.action == 'edit':
        edit_reg_task(args, user)
    elif args.action == 'all':
        show_all_reg_tasks(user)


def process_event_action(args, user):
    if args.action == 'edit':
        edit_event(args, user)
    elif args.action == 'all':
        show_all_events(user)


def login(args, user_session):
    if user_session.login_user(args.username) is not None:
        print('User {} logged in'.format(args.username))
    else:
        print('User {} does not exist'.format(args.username), file=sys.stderr)


def logout(user_session):
    user_session.logout_user()
    print('User logged out')


def add_user(args, user_session):
    user = User(username=args.username)
    if UserDatabase().create(user):
        user_session.login_user(user.username)
        print('User {} registered'.format(args.username))
    else:
        print('Error. User {} already exists'.format(
            args.username), file=sys.stderr)


def show_user(user):
    print("ID: {} USER: {}".format(user.id, user.username))


def show_all_users():
    users = UserDatabase().all_users()
    for user in users:
        print("ID: {} USER: {}".format(user.id, user.username))


def add_tag(args, user):
    tag = Tag(name=args.name)
    commands.add_tag(create_tag_controller(user), tag)
    print('Tag "{}" added'.format(args.name))


def edit_tag(args, user):
    tag = commands.get_tag_by_id(create_tag_controller(user), args.id)
    if tag is None:
        print("There is no tag with such id")
        quit()
    tag.name = args.name
    commands.update_tag(create_tag_controller(user), tag)
    print('Tag {} with id {} was updated'.format(tag.name, tag.id))


def show_tag(args, user):
    tag = commands.get_tag_by_id(create_tag_controller(user), args.id)
    print("ID: {} TAG: {}".format(tag.id, tag.name))


def delete_tag(args, user):
    commands.delete_tag(create_tag_controller(user), args.id)
    print("Tag was deleted")


def show_all_tags(user):
    tags = commands.user_tags(create_tag_controller(user))
    for tag in tags:
        print("ID: {} TAG: {}".format(tag.id, tag.name))


def validate_time_in_task(begin, end):
    if begin is not None and end is not None:
        if begin > end:
            print("Error: task begins later than ends", file=sys.stderr)
            quit()


def add_task(args, user):
    task_id = None
    task = Task(title=args.title)
    if args.note is not None:
        task.note = args.note
    if args.begin is not None:
        task.begin = dateparser.parse(args.begin)
    if args.end is not None:
        task.end = dateparser.parse(args.end)
        validate_time_in_task(task.begin, task.end)
    if args.tag_id is not None:
        task.tag_id = args.tag_id
    if args.priority is not None:
        task.priority = TaskPriority[args.priority.upper()].value
    if args.regular is not None:
        parsed_time = dateparser.parse(args.regular)
        if parsed_time is None:
            print("Error. Repeat time is incorrect", file=sys.stderr)
            quit()
        else:
            interval = (datetime.datetime.now() - parsed_time).total_seconds()
            updating_date = datetime.datetime.now() - datetime.timedelta(seconds=interval)
            if interval < MIN_INTERVAL:
                print("Error. Task's interval is incorrect", file=sys.stderr)
            else:
                if args.start_date is not None:
                    begin_date = dateparser.parse(args.start_date)
                    if begin_date is not None:
                        time_delta = (
                            interval - (begin_date - datetime.datetime.now()).total_seconds())
                        updating_date = datetime.datetime.now() - datetime.timedelta(seconds=time_delta)
            task.status = TaskStatus.NEW.value
            task_id = commands.add_task(create_task_controller(user), task).id
            reg = RegTask(task_id=task_id, user_id=user.id,
                          interval=interval, updating_date=updating_date)
            commands.add_reg_task(create_reg_task_controller(user), reg)
            print('Regular task added')
    else:
        task_id = commands.add_task(create_task_controller(user), task).id
        print('Task added')
    if args.event is not None:
        event = Event(user_id=user.id, task_id=task_id,
                      place=args.event)
        commands.add_event(create_event_controller(user), event)


def edit_task(args, user):
    task = commands.get_task_by_id(create_task_controller(user), args.id)
    if task is not None:
        if args.title is not None:
            task.title = args.title
        if args.note is not None:
            task.note = args.note
        if args.begin is not None:
            task.begin = dateparser.parse(args.begin)
        if args.end is not None:
            task.end = dateparser.parse(args.end)
            validate_time_in_task(task.begin, task.end)
        if args.tag_id is not None:
            task.tag_id = args.tag_id
        if args.priority is not None:
            task.priority = TaskPriority[args.priority.upper()].value
        commands.update_task(create_task_controller(user), task)
        print("Task was updated")
    else:
        print("Error. There is no task with such id", file=sys.stderr)


def show_task(args, user):
    task = commands.get_task_by_id(create_task_controller(user), args.id)
    print_task(task, user)


def show_all_tasks(user):
    tasks = commands.user_tasks(create_task_controller(user))
    print_task_list(tasks, user)


def show_assigned_tasks(user):
    tasks = commands.assigned_tasks(create_task_controller(user))
    print_task_list(tasks, user)


def show_new_tasks(user):
    print('NEW:')
    tasks = commands.tasks_with_status(
        create_task_controller(user), TaskStatus.NEW.value)
    print_task_list(tasks, user)


def show_in_work_tasks(user):
    print('IN_WORK:')
    tasks = commands.tasks_with_status(
        create_task_controller(user), TaskStatus.IN_WORK.value)
    print_task_list(tasks, user)


def show_done_tasks(user):
    print('DONE:')
    tasks = commands.tasks_with_status(
        create_task_controller(user), TaskStatus.DONE.value)
    print_task_list(tasks, user)


def show_can_read_tasks(user):
    tasks = commands.can_read_tasks(create_task_controller(user))
    print_task_list(tasks, user)


def show_can_write_tasks(user):
    tasks = commands.can_write_tasks(create_task_controller(user))
    print_task_list(tasks, user)


def set_task_as_new(args, user):
    commands.set_task_status(create_task_controller(
        user), args.id, TaskStatus.NEW.value)


def set_task_as_in_work(args, user):
    commands.set_task_status(create_task_controller(
        user), args.id, TaskStatus.IN_WORK.value)


def set_task_as_done(args, user):
    commands.set_task_status(create_task_controller(
        user), args.id, TaskStatus.DONE.value)


def delete_task(args, user):
    try:
        commands.delete_task(create_task_controller(user), args.id)
        print('Task id {} deleted'.format(args.id))
    except UserHasNoRightError:
        print("User has no rights for this action", file=sys.stderr)


def create_sub_task(args, user):
    task = Task(title=args.title)
    if args.note is not None:
        task.note = args.note
    if args.begin is not None:
        task.begin = dateparser.parse(args.begin)
    if args.end is not None:
        task.end = dateparser.parse(args.end)
        validate_time_in_task(task.begin, task.end)
    if args.tag_id is not None:
        task.tag_id = args.tag_id
    if args.priority is not None:
        task.priority = TaskPriority[args.priority.upper()].value
    commands.create_sub_task(
        create_task_controller(user), args.parent_task_id, task)
    print("Sub task was created")


def show_sub_task(args, user):
    tasks = commands.get_sub_tasks(
        create_task_controller(user), args.pd, True)
    print_task_list(tasks, user)


def show_parent_task(args, user):
    task = commands.get_parent_task(create_task_controller(user), args.id)
    print_task(task, user)


def assign_task_on_user(args, user):
    commands.assign_task_on_user(
        task_controller=create_task_controller(user),
        task_id=args.task_id,
        user_id=args.user_id)


def add_user_for_read(args, user):
    commands.add_user_for_read(
        task_controller=create_task_controller(user),
        user_id=args.user_id,
        task_id=args.task_id)


def add_user_for_write(args, user):
    commands.add_user_for_write(
        task_controller=create_task_controller(user),
        user_id=args.user_id,
        task_id=args.task_id)


def remove_user_for_read(args, user):
    commands.remove_user_for_read(
        task_controller=create_task_controller(user),
        user_id=args.user_id,
        task_id=args.task.id)


def remove_user_for_write(args, user):
    commands.remove_user_for_write(
        task_controller=create_task_controller(user),
        user_id=args.user_id,
        task_id=args.task.id)


def edit_reg_task(args, user):
    reg = commands.get_reg_task_by_id(
        create_reg_task_controller(user), args.id)
    if reg is not None:
        if args.regular is not None:
            parsed_time = dateparser.parse(args.regular)
            if parsed_time is None:
                print("Error. Repeat time is incorrect", file=sys.stderr)
            else:
                interval = (datetime.datetime.now() -
                            parsed_time).total_seconds()
                updating_date = datetime.datetime.now() - datetime.timedelta(seconds=interval)
                if interval < MIN_INTERVAL:
                    print("Error. Task's interval is incorrect", file=sys.stderr)
                else:
                    reg.interval = interval
        if args.start_date is not None:
            start_date = dateparser.parse(args.start_date)
            if start_date is not None:
                time_delta = (
                    interval - (start_date - datetime.datetime.now()).total_seconds())
                updating_date = datetime.datetime.now() - datetime.timedelta(seconds=time_delta)
                reg.updating_date = updating_date
        commands.update_reg_task(create_reg_task_controller(user), reg)
        print("Updated reg task")
    else:
        print("Error. There is no regular task with such id", file=sys.stderr)


def show_all_reg_tasks(user):
    regs = commands.get_reg_tasks(create_reg_task_controller(user))
    print_reg_task_list(regs)


def edit_event(args, user):
    event = commands.get_event_by_id(create_event_controller(user), args.id)
    if event is not None:
        if args.place is not None:
            event.place = args.place
        commands.update_event(create_event_controller(user), event)
        print("Event was updated")
    else:
        print("Error. There is no event with such id", file=sys.stderr)


def show_all_events(user):
    events = commands.user_events(create_event_controller(user))
    print_event_list(events)


def create_tag_controller(user, database_name=config.DATABASE):
    return mt_library.controllers.tags_controller.create_tags_controller(user.id, database_name)


def create_task_controller(user, database_name=config.DATABASE):
    return mt_library.controllers.tasks_controller.create_task_controller(user.id, database_name)


def create_reg_task_controller(user, database_name=config.DATABASE):
    return mt_library.controllers.reg_tasks_controller.create_reg_tasks_controller(user.id, database_name)


def create_event_controller(user, database_name=config.DATABASE):
    return mt_library.controllers.events_controller.create_events_controller(user.id, database_name)


def print_task(task, user):
    if task is not None:
        result = "ID: {}\n".format(task.id)
        if task.parent_id is not None:
            result += "PARENT : {}\n".format(task.parent_id)
        if task.assigned_user_id is not None:
            result += "ASSIGNED USER: {}\n".format(task.assigned_user_id)
        result += "TITLE: {}\n".format(task.title)
        if task.note is not "":
            result += "NOTE: {}\n".format(task.note)
        if task.begin is not None:
            result += "BEGIN: {}\n".format(task.begin)
        if task.end is not None:
            result += "END: {}\n".format(task.end)
        if task.tag_id is None:
            result += "TAG: None\n"
        else:
            result += "TAG: {}\n".format(commands.get_tag_by_id(
                create_tag_controller(user), task.tag_id).name)
        result += "PRIORITY: {}\nSTATUS: {}\n".format(
            TaskPriority(task.priority).name, TaskStatus(task.status).name)
        result += "CREATED : {}\nUPDATED : {}\n".format(
            task.creating_date, task.updating_date)
        print(result)


def print_reg_task(reg):
    print("ID: {}\nTASK: {}\nINTERVAL: {}\nUPDATING DATE: {}".format(
        reg.id,
        reg.task_id,
        humanize.naturaldelta(datetime.timedelta(seconds=reg.interval)),
        reg.updating_date))


def print_event(event):
    print("ID: {}\nTASK: {}\nPLACE: {}".format(
        event.id,
        event.task_id,
        event.place))


def print_task_list(task_list, user):
    if task_list is not None:
        for task in task_list:
            print_task(task, user)


def print_reg_task_list(regs):
    if regs is not None:
        for reg in regs:
            print_reg_task(reg)


def print_event_list(events):
    if events is not None:
        for event in events:
            print_event(event)


def handle_commands():
    user_session = UserSession(
        user_session_file=config.CONFIG_FILE,
        database_name=config.DATABASE)
    current_user = user_session.get_current_user()
    if current_user is not None:
        task_controller = create_task_controller(current_user)
        create_reg_task_controller(current_user).create_task_with_reg(
            task_controller)
        create_event_controller(current_user).create_task_with_event(task_controller)

    parser = init_parser()
    args = parser.parse_args()
    process_object(args, user_session)
