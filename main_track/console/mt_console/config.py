import os

APP_DATA_DIRECTORY = os.path.join(os.environ['HOME'], 'main_track')
DATABASE = os.path.join(APP_DATA_DIRECTORY, 'main_track.db')
CONFIG_FILE = os.path.join(APP_DATA_DIRECTORY, 'config.ini')
LOGIN_ENABLED = True
LOGS_DIRECTORY = APP_DATA_DIRECTORY
LOG_FILE = 'log.log'
LOG_ALL_LEVELS = True
LOG_FORMAT = '%(asctime)s, %(name)s, [%(levelname)s]: %(message)s'
