import configparser
from .user import UserDatabase


class UserSession:
    def __init__(self, user_session_file, database_name):
        self.user_session_file = user_session_file
        self.config_parser = configparser.ConfigParser()
        self.database_name = database_name

    def login_user(self, username):
        user = UserDatabase(self.database_name).get_by_username(username)
        if user is not None:
            self.config_parser['user'] = {}
            self.config_parser['user']['username'] = user.username
            with open(self.user_session_file, 'w') as configfile:
                self.config_parser.write(configfile)
            return user

    def logout_user(self):
        self.config_parser['user'] = {}
        with open(self.user_session_file, 'w') as configfile:
            self.config_parser.write(configfile)

    def get_current_user(self):
        username = None
        try:
            self.config_parser.read(self.user_session_file)
            username = self.config_parser['user']['username']
        except BaseException:
            pass
        return self.login_user(username)
