from setuptools import setup, find_packages


setup(
      name='main_track_cli',
      version='1.0',
      install_requires=['dateparser', 'humanize'],
      packages=find_packages(),
      entry_points='''[console_scripts]
      main_track=mt_console.main:main''')
