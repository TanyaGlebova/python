$(document).ready(function () {
                var table = $('#dtBasicExample').DataTable({
                    'columnDefs': [{
                            'targets': 0,
                            'checkboxes': {
                               'selectRow': true
                                }
                            }],
                    'select': {
                            'style': 'multi'
                            },
                    'dom': '<"toolbar">frtip',
                    'order': [[1, 'asc']]
                });
                $("div.toolbar").html('<a href="{% url "new_task" %}"  class="btn btn-primary">New</a> <input type="submit" class="btn btn-dark" id="edit" name="_edit" disabled value="Edit"/> <input type="submit" class="btn btn-danger" id="delete" name="_delete" disabled value="Delete"/> ');
                $('#dtBasicExample tbody input[type="checkbox"]').change(function() {
                    var count = $('#dtBasicExample tbody :input[type="checkbox"]:checked').length;
                    if (this.checked){
                        var data = table.row($(this).closest('tr')).data()[0];
                        this.name="checkbox";
                        var temp=data.split('id="')[1];
                        this.value=temp.split('"')[0];
                    }
                    if (count > 0) {
                        $('#delete').removeAttr('disabled');
                        if (count == 1) {
                            $('#edit').removeAttr('disabled');
                        }
                        else {
                             $('#edit').attr('disabled', true);
                        }
                    }
                    else {
                        $('#delete').attr('disabled', true);
                        $('#edit').attr('disabled', true);
                    }
                });

            });