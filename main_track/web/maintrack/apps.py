from django.apps import AppConfig


class MaintrackConfig(AppConfig):
    name = 'maintrack'
