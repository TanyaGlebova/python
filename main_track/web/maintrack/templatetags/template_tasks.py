from django import template
from mt_library.controllers.tasks_controller import create_task_controller
from mt_library.commands import get_task_by_id
from mt_library.database.db_task import DbTask
from django.conf import settings

register = template.Library()


@register.simple_tag
def get_template_task_title(task_id, user_id):
    controller = create_task_controller(user_id, settings.MAIN_TRACK_DATABASE_PATH)
    print('get task: {0}'.format(task_id))
    task = get_task_by_id(controller, task_id)
    print(task.title)
    return task.title
