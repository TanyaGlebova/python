from django import template
import datetime

register = template.Library()


@register.simple_tag
def time_format(interval):
    return str(datetime.timedelta(seconds=interval))