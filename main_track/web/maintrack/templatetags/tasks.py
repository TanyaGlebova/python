from django import template
from mt_library.entities.task import TaskStatus, TaskPriority
from mt_library.controllers.tags_controller import create_tags_controller
from mt_library.controllers.tasks_controller import create_task_controller
from mt_library.commands import get_task_by_id, get_tag_by_id
from django.conf import settings


register = template.Library()


@register.simple_tag
def get_status(status):
    return TaskStatus(status).name


@register.simple_tag
def get_priority(priority):
    return TaskPriority(priority).name


@register.simple_tag
def get_tag(task_id, user_id):
    task = get_task_by_id(create_task_controller(user_id, settings.MAIN_TRACK_DATABASE_PATH), task_id)
    tag = get_tag_by_id(create_tags_controller(user_id, settings.MAIN_TRACK_DATABASE_PATH), task.tag_id)
    if tag is not None:
        return tag.name
    else:
        return None


@register.simple_tag
def get_parent(task_id, user_id):
    task = get_task_by_id(create_task_controller(user_id, settings.MAIN_TRACK_DATABASE_PATH), task_id)
    parent_task = get_task_by_id(create_task_controller(user_id, settings.MAIN_TRACK_DATABASE_PATH), task.parent_id)
    if parent_task is not None:
        return parent_task.title
    else:
        return None


@register.simple_tag
def have_parent(task_id, user_id):
    task = get_task_by_id(create_task_controller(user_id, settings.MAIN_TRACK_DATABASE_PATH), task_id)
    if task.parent_id is not None:
        return True
    else:
        return False

