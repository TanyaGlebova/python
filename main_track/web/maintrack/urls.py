from . import views
from django.conf.urls import include, url
from django.contrib.auth import views as auth_views


tasks_patterns = [
    url(r'^new/$', views.create_task, name='new_task'),
    url(r'^all/$', views.all_tasks, name='all_tasks'),
    url(r'^(?P<id>[0-9]+)/$', views.detail_task, name='detail_task'),
]

regulars_patterns = [
    url(r'^all/$', views.all_regular_tasks, name='all_regular_tasks'),
    url(r'^new/$', views.create_regular_task, name='new_regular_task'),
    url(r'^(?P<id>[0-9]+)/$', views.detail_regular_task, name='detail_regular_task'),
]

events_patterns = [
    url(r'^all/$', views.all_events, name='all_events'),
    url(r'^new/$', views.create_event, name='new_event'),
    url(r'^(?P<id>[0-9]+)/$', views.detail_event, name='detail_event'),
]

tags_patterns = [
    url(r'^all/$', views.all_tags, name='all_tags'),
    url(r'^new/$', views.create_tag, name='new_tag'),
    url(r'^(?P<id>[0-9]+)/$', views.detail_tag, name='detail_tag'),
]

urlpatterns = [

    url(r'^tasks/', include(tasks_patterns)),
    url(r'^regulars/', include(regulars_patterns)),
    url(r'^events/', include(events_patterns)),
    url(r'^tags/', include(tags_patterns)),
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': 'login'}, name='logout'),
    url(r'^signup/$', views.sign_up, name='signup'),
    url(r'^home/', views.home, name='home'),
    url(r'^help/', views.main_track_help, name='help'),
]

