import datetime
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import redirect, render
from django.conf import settings
from mt_library.entities.task import Task, TaskStatus
from mt_library.entities.reg_task import RegTask
from mt_library.entities.event import Event
from mt_library.entities.tag import Tag
from mt_library.database.db_entities import Task as DbTask
from mt_library.controllers.tasks_controller import create_task_controller
from mt_library.controllers.reg_tasks_controller import create_reg_tasks_controller
from mt_library.controllers.events_controller import create_events_controller
from mt_library.controllers.tags_controller import create_tags_controller
from mt_library.commands import (
    add_task,
    add_reg_task,
    add_event,
    add_tag,
    filter_tasks,
    get_task_by_id,
    get_reg_task_by_id,
    get_reg_tasks,
    get_event_by_id,
    user_events,
    get_tag_by_id,
    user_tags,
    user_can_write_task,
    update_task,
    update_reg_task,
    update_event,
    update_tag,
    delete_task,
    delete_event,
    delete_reg_task,
    delete_tag,
    user_tasks,
)
from mt_library.exceptions.exceptions import InvalidTaskTimeError
from .forms import TaskForm, RegularTaskForm, EventForm, TagForm
from pytimeparse import parse


def _create_task_controller(user_id):
    return create_task_controller(
        user_id, settings.MAIN_TRACK_DATABASE_PATH)


def _create_regular_task_controller(user_id):
    return create_reg_tasks_controller(
        user_id, settings.MAIN_TRACK_DATABASE_PATH)


def _create_event_controller(user_id):
    return create_events_controller(
        user_id, settings.MAIN_TRACK_DATABASE_PATH)


def _create_tag_controller(user_id):
    return create_tags_controller(
        user_id, settings.MAIN_TRACK_DATABASE_PATH)


def process_regulars(function):
    def wrap(request, *args, **kwargs):
        _create_regular_task_controller(
            request.user.id).create_task_with_reg(_create_task_controller(
                request.user.id))
        return function(request, *args, **kwargs)
    return wrap


@login_required
@process_regulars
def create_task(request):
    if request.method == 'POST':
        form = TaskForm(request.user.id, request.POST)
        if form.is_valid():
            title = form.cleaned_data['title']
            note = form.cleaned_data['note']
            parent_id = form.cleaned_data['parent_id']
            parent_id = None if not parent_id else parent_id
            assigned_user = form.cleaned_data['assigned_user']
            assigned_user = request.user.id if not assigned_user else assigned_user.id
            begin = form.cleaned_data['begin']
            end = form.cleaned_data['end']
            status = form.cleaned_data['status']
            tag = form.cleaned_data['tag']
            tag = None if not tag else tag
            priority = form.cleaned_data['priority']
            task = Task(
                title=title,
                note=note,
                parent_id=parent_id,
                user_id=request.user.id,
                assigned_user_id=assigned_user,
                begin=begin,
                end=end,
                status=status,
                tag_id=tag,
                priority=priority)
            task_controller = _create_task_controller(request.user.id)
            try:
                task.id = add_task(task_controller, task).id
            except InvalidTaskTimeError:
                form.add_error(
                    'begin',
                    'Time is invalid: task begin later than end')
                return render(request,
                              'tasks/new.html',
                              {'form': form,
                               'user': request.user})
            return redirect('/tasks/all')
    else:
        tag = None
        parent_id = None
        if request.GET.get('tag') is not None:
            tag = request.GET.get('tag')
        if request.GET.get('parent_id') is not None:
            parent_id = request.GET.get('parent_id')
        form = TaskForm(request.user.id)
        if tag is not None:
            form.fields["tag"].initial = tag
        if parent_id is not None:
            form.fields["parent_id"].initial = parent_id
    return render(request,
                  'tasks/new.html',
                  {'form': form,
                   'user': request.user})


@login_required
@process_regulars
def detail_task(request, id):
    task_controller = _create_task_controller(request.user.id)
    task = get_task_by_id(task_controller, id)
    if task is None or not user_can_write_task(task_controller, id):
        return redirect('/tasks/all')
    if request.method == 'POST':
        form = TaskForm(request.user.id, request.POST)
        if form.is_valid():
            task.title = form.cleaned_data['title']
            task.note = form.cleaned_data['note']
            parent_id = form.cleaned_data['parent_id']
            task.parent_id = None if not parent_id else parent_id
            assigned_user = form.cleaned_data['assigned_user']
            task.assigned_user_id = None if not assigned_user else assigned_user.id
            task.begin = form.cleaned_data['begin']
            task.end = form.cleaned_data['end']
            task.status = form.cleaned_data['status']
            tag_id = form.cleaned_data['tag']
            task.tag_id = None if not tag_id else tag_id
            task.priority = form.cleaned_data['priority']
            try:
                update_task(task_controller, task)
            except InvalidTaskTimeError:
                form.add_error(
                    'begin',
                    'Time is invalid: task begin later than end')
                return render(request,
                              'tasks/detail.html',
                              {'form': form,
                               'user': request.user})
            return redirect('/tasks/all')
    else:
        form = TaskForm(
            request.user.id,
            initial={'title': task.title,
                     'note': task.note,
                     'parent_id': task.parent_id,
                     'assigned_user': task.assigned_user_id,
                     'begin': task.begin,
                     'end': task.end,
                     'status': task.status,
                     'tag': task.tag_id,
                     'priority': task.priority})
    return render(request,
                  'tasks/detail.html',
                  {'form': form,
                   'user': request.user})


@login_required
@process_regulars
def all_tasks(request):
    task_controller = _create_task_controller(request.user.id)
    if request.method == 'POST':
        request_dict = dict(request.POST)
        if '_delete' in request.POST:
            try:
                delete_task_id = request_dict['checkbox']
                for id in delete_task_id:
                    delete_task(task_controller, id)
            except KeyError:
                pass
        elif '_edit' in request.POST:
            try:
                edit_task_id = request_dict['checkbox']
                return redirect('/tasks/' + edit_task_id[0])
            except KeyError:
                pass
    overdue_tasks = filter_tasks(task_controller, (DbTask.status == TaskStatus.DONE.value) | (DbTask.end < datetime.datetime.now()))
    for overdue in overdue_tasks:
        delete_task(task_controller, overdue.id)
    tasks = filter_tasks(task_controller, ((DbTask.user_id == request.user.id) or
                                           (DbTask.assigned_user_id == request.user.id)))
    return render(request,
                  'tasks/all.html',
                  {'tasks': tasks,
                   'user': request.user})


@login_required
@process_regulars
def create_regular_task(request):
    task_controller = _create_task_controller(request.user.id)
    regular_task_controller = _create_regular_task_controller(request.user.id)
    if request.method == 'POST':
        regular_form = RegularTaskForm(request.user.id, request.POST)
        str_interval = regular_form.data['interval']
        interval = parse(str_interval)
        if interval is None:
            regular_form.add_error('interval', "Interval is incorrect")
        elif interval < 300:
            regular_form.add_error('interval', "Interval should be more then 5 minutes")
        if regular_form.is_valid():
            template_task_id = regular_form.cleaned_data['template_task_id']
            if template_task_id is None:
                template_task_id = user_tasks(task_controller)[-1].id
            task = get_task_by_id(task_controller, template_task_id)
            if task.updating_date is None:
                task.updating_date = datetime.datetime.now() - datetime.timedelta(seconds=interval)
            regular_task = RegTask(
                interval=interval,
                user_id=request.user.id,
                task_id=template_task_id,
                updating_date=task.updating_date)
            update_task(task_controller, task)
            add_reg_task(regular_task_controller, regular_task)
        return redirect('/regulars/all')
    else:
        regular_form = RegularTaskForm(request.user.id)
    return render(request,
                  'regulars/new.html',
                  {'regular_form': regular_form,
                   'user': request.user})


@login_required
@process_regulars
def detail_regular_task(request, id):
    regular_task_controller = _create_regular_task_controller(request.user.id)
    regular = get_reg_task_by_id(regular_task_controller, id)
    if request.method == 'POST':
        form = RegularTaskForm(request.user.id, request.POST)
        str_interval = form.data['interval']
        interval = parse(str_interval)
        if interval is None:
            form.add_error('interval', "Interval is incorrect")
        elif interval < 300:
            form.add_error('interval', "Interval should be more then 5 minutes")
        if form.is_valid():
            updating_date = datetime.datetime.now()
            regular.updating_date = updating_date
            regular.interval = interval
            update_reg_task(regular_task_controller, regular)
            return redirect('/regulars/all')
    else:
        form = RegularTaskForm(request.user.id,
                               initial={'interval': datetime.timedelta(seconds=regular.interval)})
    return render(request,
                  'regulars/detail.html',
                  {'regular_form': form,
                   'regular': regular,
                   'user': request.user})


@login_required
@process_regulars
def all_regular_tasks(request):
    regular_task_controller = _create_regular_task_controller(request.user.id)
    if request.method == 'POST':
        request_dict = dict(request.POST)
        if '_delete' in request.POST:
            try:
                delete_task_id = request_dict['checkbox']
                for id in delete_task_id:
                    delete_reg_task(regular_task_controller, id)
            except KeyError:
                pass
        elif '_edit' in request.POST:
            try:
                edit_task_id = request_dict['checkbox']
                return redirect('/regulars/' + edit_task_id[0])
            except KeyError:
                pass
    regulars = get_reg_tasks(regular_task_controller)
    return render(request, 'regulars/all.html',
                  {'regulars': regulars,
                   'user': request.user})


@login_required
@process_regulars
def create_event(request):
    event_controller = _create_event_controller(request.user.id)
    if request.method == 'POST':
        form = EventForm(request.user.id, request.POST)
        place = form.data['place']
        if form.is_valid():
            template_task_id = form.cleaned_data['template_task_id']
            event = Event(
                user_id=request.user.id,
                task_id=template_task_id,
                place=place)
            add_event(event_controller, event)
            return redirect('/events/all')
    else:
        form = EventForm(request.user.id)
    return render(request,
                  'events/new.html',
                  {'form': form,
                   'user': request.user})


@login_required
@process_regulars
def detail_event(request, id):
    event_controller = _create_event_controller(request.user.id)
    event = get_event_by_id(event_controller, id)
    if request.method == 'POST':
        form = EventForm(request.user.id, request.POST)
        place = form.data['place']
        if form.is_valid():
            event.place = place
            update_event(event_controller, event)
            return redirect('/events/all')
    else:
        form = EventForm(request.user.id,
                         initial={'place': event.place})
    return render(request,
                  'events/detail.html',
                  {'form': form,
                   'user': request.user})


@login_required
@process_regulars
def all_events(request):
    event_controller = _create_event_controller(request.user.id)
    if request.method == 'POST':
        request_dict = dict(request.POST)
        if '_delete' in request.POST:
            try:
                delete_task_id = request_dict['checkbox']
                for id in delete_task_id:
                    delete_event(event_controller, id)
            except KeyError:
                pass
        elif '_edit' in request.POST:
            try:
                edit_task_id = request_dict['checkbox']
                return redirect('/events/' + edit_task_id[0])
            except KeyError:
                pass
    events = user_events(event_controller)
    return render(request, 'events/all.html',
                  {'events': events,
                   'user': request.user})


@login_required
@process_regulars
def create_tag(request):
    if request.method == 'POST':
        form = TagForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            tag = Tag(name)
            tag_controller = _create_tag_controller(request.user.id)
            add_tag(tag_controller, tag)
            return redirect('/tags/all')
    else:
        form = TagForm()
    return render(request,
                  'tags/new.html',
                  {'form': form,
                   'user': request.user})


@login_required
@process_regulars
def detail_tag(request, id):
    tag_controller = _create_tag_controller(request.user.id)
    tag = get_tag_by_id(tag_controller, id)
    if tag is None:
        return redirect('/tags')
    if request.method == 'POST':
        form = TagForm(request.POST)
        if form.is_valid():
            tag.name = form.cleaned_data['name']
            update_tag(tag_controller, tag)
            return redirect('/tags/all')
    else:
        form = TagForm(initial={'name': tag.name})
    return render(request,
                  'tags/detail.html',
                  {'form': form,
                   'user': request.user})


@login_required
@process_regulars
def all_tags(request):
    tag_controller = _create_tag_controller(request.user.id)
    if request.method == 'POST':
        request_dict = dict(request.POST)
        if '_delete' in request.POST:
            try:
                delete_task_id = request_dict['checkbox']
                for id in delete_task_id:
                    delete_tag(tag_controller, id)
            except KeyError:
                pass
        elif '_edit' in request.POST:
            try:
                edit_task_id = request_dict['checkbox']
                return redirect('/tags/' + edit_task_id[0])
            except KeyError:
                pass
    tags = user_tags(tag_controller)
    return render(request,
                  'tags/all.html',
                  {'tags': tags,
                   'user': request.user})


@login_required
def home(request):
    task_controller = _create_task_controller(request.user.id)
    tasks = filter_tasks(task_controller, ((DbTask.user_id == request.user.id) or
                                           (DbTask.assigned_user_id == request.user.id)) and
                         (DbTask.begin > datetime.date.today()) and
                         (DbTask.begin < datetime.date.today() + datetime.timedelta(days=1)))
    return render(request,
                  'home.html',
                  {'tasks': tasks,
                   'user': request.user})


def sign_up(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/login/')
    else:
        form = UserCreationForm()
    return render(request,
                  'registration/signup.html',
                  {'form': form,
                   'user': request.user})


@login_required
def main_track_help(request):
    return render(request,
                  'help.html',
                  {'user': request.user})
