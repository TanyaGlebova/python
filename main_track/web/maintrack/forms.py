import datetime
from django import forms
from django.conf import settings
from django.contrib.auth.models import User
from mt_library.entities.task import TaskStatus, TaskPriority
from mt_library.database.db_entities import Task
from mt_library.controllers.tasks_controller import create_task_controller
from mt_library.controllers.tags_controller import create_tags_controller
import mt_library.commands as commands
from tempus_dominus.widgets import DateTimePicker


class TaskForm(forms.Form):
    def __init__(self, user_id=None, *args, **kwargs):
        super(TaskForm, self).__init__(*args, **kwargs)
        tasks = commands.filter_tasks(create_task_controller(user_id, settings.MAIN_TRACK_DATABASE_PATH),
                                      (Task.user_id == user_id))
        tasks_tuple = [(task.id, task.title) for task in tasks]
        tags = commands.user_tags(create_tags_controller(user_id, settings.MAIN_TRACK_DATABASE_PATH))
        tags_tuple = [(tag.id, tag.name) for tag in tags]
        tasks_tuple.insert(0, ('', '---------'))
        tags_tuple.insert(0, ('', '---------'))

        self.fields['tag'] = forms.ChoiceField(choices=tags_tuple, required=False)
        self.fields['parent_id'] = forms.ChoiceField(choices=tasks_tuple, required=False)

    title = forms.CharField(max_length=60, widget=forms.TextInput(attrs={'title': "Task's title"}))
    note = forms.CharField(max_length=400, widget=forms.Textarea(attrs={'title': "Task's note"}))
    parent_id = forms.ChoiceField(required=False)
    assigned_user = forms.ModelChoiceField(queryset=User.objects.all(), required=False)
    begin = forms.DateTimeField(
        widget=DateTimePicker(
            options={
                'minDate': (datetime.date.today()).strftime('%Y-%m-%d'),
                'useCurrent': True,
            }
        ), required=True
    )
    end = forms.DateTimeField(
        widget=DateTimePicker(
            options={
                'minDate': (datetime.date.today()).strftime('%Y-%m-%d'),
                'useCurrent': True,
            }
        ), required=True
    )
    status = forms.ChoiceField(choices=[(e.value, TaskStatus(e).name) for e in TaskStatus], required=False)
    tag = forms.ChoiceField(required=False)
    priority = forms.ChoiceField(choices=[(e.value, TaskPriority(e).name) for e in TaskPriority], required=False)


class RegularTaskForm(forms.Form):
    def __init__(self, user_id=None, *args, **kwargs):
        super(RegularTaskForm, self).__init__(*args, **kwargs)
        tasks = commands.filter_tasks(create_task_controller(user_id, settings.MAIN_TRACK_DATABASE_PATH),
                                      (Task.user_id == user_id))
        tasks_tuple = [(task.id, task.title) for task in tasks]
        tasks_tuple.insert(0, ('', '---------'))
        self.fields['template_task_id'] = forms.ChoiceField(choices=tasks_tuple, required=False)

    template_task_id = forms.ChoiceField(required=False)
    interval = forms.CharField(max_length=400)


class EventForm(forms.Form):
    def __init__(self, user_id=None, *args, **kwargs):
        super(EventForm, self).__init__(*args, **kwargs)
        tasks = commands.filter_tasks(create_task_controller(user_id, settings.MAIN_TRACK_DATABASE_PATH),
                                      (Task.user_id == user_id))
        tasks_tuple = [(task.id, task.title) for task in tasks]
        tasks_tuple.insert(0, ('', '---------'))
        self.fields['template_task_id'] = forms.ChoiceField(choices=tasks_tuple, required=False)

    template_task_id = forms.ChoiceField(required=False)
    place = forms.CharField(max_length=400)


class TagForm(forms.Form):
    name = forms.CharField(max_length=100)


