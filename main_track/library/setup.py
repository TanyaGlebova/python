from setuptools import setup, find_packages

setup(
    name='main_track',
    version='0.1',
    install_requires=['peewee', 'factory_boy'],
    packages=find_packages(),
    test_suite='tests.run_test')
