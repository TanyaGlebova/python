import unittest


def run_test():
    modules = ['tests.entities_test',
               'tests.database_test',
               'tests.controllers_test']

    suite = unittest.TestSuite()
    for m in modules:
        suite.addTests(unittest.defaultTestLoader.loadTestsFromName(m))
    unittest.TextTestRunner().run(suite)


if __name__ == "tests.run_test":
    run_test()
