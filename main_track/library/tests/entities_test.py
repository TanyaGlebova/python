import unittest
import datetime

from mt_library.entities.task import Task
from mt_library.entities.tag import Tag
from mt_library.entities.reg_task import RegTask
from mt_library.entities.event import Event


class EntitiesTest(unittest.TestCase):
    def create_task(self,
                    title="something",
                    note="wrote something",
                    id=1,
                    user_id=1):
        self.task_title = title
        self.task_note = note
        return Task(title=title,
                    note=note,
                    id=id,
                    user_id=user_id)

    def test_task_creation(self):
        task = self.create_task()
        self.assertIsInstance(task, Task)
        self.assertEqual(task.title, self.task_title)
        self.assertEqual(task.note, self.task_note)

    def create_tag(self, name="homework"):
        self.tag_name = name
        return Tag(name)

    def test_tag_creation(self):
        tag = self.create_tag()
        self.assertIsInstance(tag, Tag)
        self.assertEqual(tag.name, self.tag_name)

    def create_reg_task(self,
                        interval=650,
                        user_id=1,
                        task_id=1,
                        updating_date=datetime.datetime.now()):
        self.interval = interval
        self.user_id = user_id
        self.task_id = task_id
        self.updating_date = updating_date
        return RegTask(interval=interval,
                       user_id=user_id,
                       task_id=task_id,
                       updating_date=updating_date)

    def test_reg_task_creation(self):
        reg = self.create_reg_task()
        self.assertEqual(reg.interval, self.interval)
        self.assertEqual(reg.user_id, self.user_id)
        self.assertEqual(reg.task_id, self.task_id)
        self.assertEqual(reg.updating_date, self.updating_date)

    def create_event(self,
                     user_id=1,
                     task_id=1,
                     place="some place"):
        self.user_id = user_id
        self.task_id = task_id
        self.place = place
        return Event(user_id=user_id,
                     task_id=task_id,
                     place=place)

    def test_event_creation(self):
        event = self.create_event()
        self.assertEqual(event.user_id, self.user_id)
        self.assertEqual(event.task_id, self.task_id)
        self.assertEqual(event.place, self.place)
