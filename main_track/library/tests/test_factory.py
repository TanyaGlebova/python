import datetime
import factory.fuzzy
from mt_library.entities.task import Task as TaskObject
from mt_library.entities.tag import Tag as TagObject
from mt_library.entities.reg_task import RegTask as RegTaskObject
from mt_library.entities.event import Event as EventObject


class TagFactory(factory.Factory):
    class Meta:
        model = TagObject
    name = factory.Faker('word')
    user_id = 8
    id = 8


class TaskFactory(factory.Factory):
    class Meta:
        model = TaskObject
    title = factory.Faker('word')
    note = factory.Faker('word')
    user_id = 8


class RegTaskFactory(factory.Factory):
    class Meta:
        model = RegTaskObject
    interval = factory.fuzzy.FuzzyInteger(300, 1000000, step=50)
    user_id = 8
    task_id = 8
    updating_date = datetime.datetime.now()


class EventFactory(factory.Factory):
    class Meta:
        model = EventObject
    user_id = 8
    task_id = 8
    place = factory.Faker('word')
