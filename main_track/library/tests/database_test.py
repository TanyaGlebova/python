import unittest
import datetime
from mt_library.database.db_entities import (
    Task,
    UserReadTasks,
    UserWriteTasks,
    Tag,
    RegTask,
    Event,
    Connector
    )
from mt_library.database.db_tag import DbTag
from mt_library.database.db_task import DbTask
from mt_library.database.db_reg_task import DbRegTask
from mt_library.database.db_event import DbEvent
from mt_library.entities.task import TaskStatus
from tests.test_factory import TaskFactory, RegTaskFactory, EventFactory, TagFactory
from mt_library.controllers.tasks_controller import TasksController


class DatabaseTest(unittest.TestCase):
    def setUp(self):
        self.database = ':memory:'
        self.db_task = DbTask(self.database)
        self.db_reg_task = DbRegTask(self.database)
        self.db_event = DbEvent(self.database)
        self.db_tag = DbTag(self.database)
        self.task = TaskFactory()
        self.tag = TagFactory()
        self.reg_task = RegTaskFactory()
        self.user_id = 8
        self.event = EventFactory()
        Connector(self.database).create_tables()

    def tearDown(self):
        Connector(self.database).drop_tables()

    """TAG TESTS"""

    def test_create_tag(self):
        before_tag_count = Tag.select().count()
        self.db_tag.create(self.tag)
        after_tag_count = Tag.select().count()
        self.assertEqual(before_tag_count+1, after_tag_count)

    def test_create_tag_and_get_it_by_id(self):
        tag_with_id = self.db_tag.create(self.tag)
        tag_from_db = self.db_tag.get_by_id(tag_with_id.id)
        self.assertEqual(tag_with_id.id, tag_from_db.id)
        self.assertEqual(tag_with_id.name, tag_from_db.name)
        self.assertEqual(tag_with_id.user_id, tag_from_db.user_id)

    def test_update_tag(self):
        tag_with_id = self.db_tag.create(self.tag)
        tag_with_id.name = "Homework"
        self.db_tag.update(tag_with_id)
        tag_from_db = Tag.get(Tag.id == tag_with_id.id)
        self.assertEqual(tag_from_db.name, "Homework")

    def test_delete_tag_by_id(self):
        tag_id = self.db_tag.create(self.tag).id
        self.db_tag.delete_by_id(tag_id)
        self.assertEqual(Tag.select().where(Tag.id == tag_id).count(), 0)

    """TASK TESTS"""

    def test_create_task(self):
        before_task_count = Task.select().count()
        self.db_task.create(self.task)
        after_task_count = Task.select().count()
        self.assertEqual(before_task_count+1, after_task_count)

    def create_task_and_get_it_by_id(self):
        task_with_id = self.db_task.create(self.task)
        task_from_db = self.db_task.get_by_id(task_with_id.id)
        self.assertEqual(task_with_id.id, task_from_db.id)
        self.assertEqual(task_with_id.title, task_from_db.title)
        self.assertEqual(task_with_id.user_id, task_from_db.user_id)
        self.assertEqual(task_with_id.note, task_from_db.note)

    def test_update_task(self):
        task_with_id = self.db_task.create(self.task)
        task_with_id.title = "Do laboratory work"
        self.db_task.update(task_with_id)
        task_from_db = Task.get(Task.id == task_with_id.id)
        self.assertEqual(task_from_db.title, "Do laboratory work")

    def test_delete_task_by_id(self):
        task_id = self.db_task.create(self.task).id
        self.db_task.delete_by_id(task_id)
        self.assertEqual(Task.select().where(Task.id == task_id).count(), 0)

    def test_return_user_tasks(self):
        task_one = TaskFactory()
        task_two = TaskFactory()
        user_id = 8
        task_one.user_id = user_id
        task_two.user_id = user_id
        self.db_task.create(task_one)
        self.db_task.create(task_two)
        tasks = self.db_task.user_task(user_id)
        self.assertEqual(len(tasks), 2)

    def test_return_sub_tasks(self):
        task_id = self.db_task.create(self.task).id
        sub_task = TaskFactory()
        sub_task.parent_id = task_id
        self.db_task.create(sub_task)
        sub_tasks = self.db_task.sub_tasks(task_id)
        self.assertEqual(len(sub_tasks), 1)

    def test_return_sub_tasks_recursive(self):
        task_id = self.db_task.create(self.task).id
        sub_task = TaskFactory()
        sub_task.parent_id = task_id
        sub_task_id = self.db_task.create(sub_task).id
        second_level_sub_task = TaskFactory()
        second_level_sub_task.parent_id = sub_task_id
        self.db_task.create(second_level_sub_task)
        sub_tasks = self.db_task.sub_tasks(task_id, True)
        self.assertEqual(len(sub_tasks), 2)

    def test_add_user_for_read(self):
        user_id = 8
        task_id = self.db_task.create(self.task).id
        self.db_task.add_user_for_read(user_id=user_id, task_id=task_id)
        self.assertEqual(UserReadTasks.select().where(UserReadTasks.task_id == task_id and
                                                      UserReadTasks.user_id == user_id).count(), 1)

    def test_add_user_for_write(self):
            user_id = 8
            task_id = self.db_task.create(self.task).id
            self.db_task.add_user_for_write(user_id=user_id, task_id=task_id)
            self.assertEqual(UserWriteTasks.select().where(
                UserWriteTasks.task_id == task_id and UserWriteTasks.user_id == user_id).count(), 1)

    def test_remove_user_for_read(self):
        user_id = 8
        task_id = self.db_task.create(self.task).id
        self.db_task.add_user_for_read(user_id=user_id, task_id=task_id)
        self.db_task.remove_user_for_read(user_id=user_id, task_id=task_id)
        self.assertEqual(UserReadTasks.select().where(UserReadTasks.task_id == task_id and
                                                      UserReadTasks.user_id == user_id).count(), 0)

    def test_remove_user_for_write(self):
        user_id = 8
        task_id = self.db_task.create(self.task).id
        self.db_task.add_user_for_write(user_id=user_id, task_id=task_id)
        self.db_task.remove_user_for_write(user_id=user_id, task_id=task_id)
        self.assertEqual(UserWriteTasks.select().where(UserWriteTasks.task_id == task_id and
                                                       UserWriteTasks.user_id == user_id).count(), 0)

    """REG TASK TESTS"""

    def test_create_reg_task(self):
        before_regs_count = RegTask.select().count()
        self.db_reg_task.create(self.reg_task)
        after_regs_count = RegTask.select().count()
        self.assertEqual(before_regs_count+1, after_regs_count)

    def test_delete_reg_task_by_id(self):
        reg_task_id = self.db_reg_task.create(self.reg_task).id
        self.db_reg_task.delete_by_id(reg_task_id)
        self.assertEqual(RegTask.select().where(RegTask.id == reg_task_id).count(), 0)

    def test_update_reg_task(self):
        new_interval = 500
        new_datetime = datetime.datetime.now()
        reg_task_with_id = self.db_reg_task.create(self.reg_task)
        reg_task_with_id.interval = new_interval
        reg_task_with_id.updating_date = new_datetime
        self.db_reg_task.update(reg_task_with_id)
        reg_task_from_db = RegTask.get(RegTask.id == reg_task_with_id.id)
        self.assertEqual(reg_task_from_db.interval, new_interval)
        self.assertEqual(reg_task_from_db.updating_date, new_datetime)

    def test_return_user_reg_tasks(self):
        user_id = 8
        self.reg_task.user_id = 8
        reg_count = 3
        for i in range(reg_count):
            self.db_reg_task.create(self.reg_task)
        regs = self.db_reg_task.user_reg_tasks(user_id)
        self.assertEqual(len(regs), reg_count)

    def test_create_task_with_regs(self):
        user_id = 8
        regular_task = TaskFactory()
        regular_task.status = TaskStatus.NEW.value
        regular_task.user_id = user_id
        task_id = self.db_task.create(regular_task).id
        before_task_count = len(self.db_task.user_task(user_id))
        interval = 300
        big_interval = interval * 10
        updating_date = datetime.datetime.now() - datetime.timedelta(seconds=interval + 5)

        repeated_reg_task = RegTask(
            user_id=user_id,
            task_id=task_id,
            updating_date=updating_date,
            interval=interval)
        repeated_reg_task_big_interval = RegTask(
            user_id=user_id,
            task_id=task_id,
            updating_date=updating_date,
            interval=big_interval)
        self.db_reg_task.create(repeated_reg_task)
        self.db_reg_task.create(repeated_reg_task_big_interval)
        self.db_reg_task.create_task_with_reg(self.db_task)
        self.assertEqual(len(self.db_task.user_task(user_id)), before_task_count + 1)

    """EVENT TESTS"""

    def test_create_event(self):
        before_event_count = Event.select().count()
        self.db_event.create(self.event)
        after_event_count = Event.select().count()
        self.assertEqual(before_event_count + 1, after_event_count)

    def test_update_event(self):
        new_place = "some place"
        event_with_id = self.db_event.create(self.event)
        event_with_id.place = new_place
        self.db_event.update(event_with_id)
        event_from_db = Event.get(Event.id == event_with_id.id)
        self.assertEqual(event_from_db.place, new_place)

    def test_delete_event_by_id(self):
        event_id = self.db_event.create(self.event).id
        self.db_event.delete_by_id(event_id)
        self.assertEqual(Event.select().where(Event.id == event_id).count(), 0)

    def test_return_user_events(self):
        user_id = 8
        self.event.user_id = 8
        events_count = 3
        for i in range(events_count):
            self.db_event.create(self.event)
        events = self.db_event.user_events(user_id)
        self.assertEqual(len(events), events_count)

    def test_create_task_with_event(self):
        task_id = self.db_task.create(self.task).id
        self.event.task_id = task_id
        self.event.place = "some place"
        event_id = self.db_event.create(self.event).id
        created_event = self.db_event.get_by_id(event_id)
        self.assertEqual(created_event.place, "some place")
