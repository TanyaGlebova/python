import datetime
import unittest
from mt_library.database.db_entities import (
    Task,
    RegTask,
    UserReadTasks,
    UserWriteTasks,
    Tag,
    Connector,
    Event
    )
from mt_library.database.db_tag import DbTag
from mt_library.database.db_event import DbEvent
from mt_library.database.db_task import DbTask
from mt_library.database.db_reg_task import DbRegTask
from mt_library.entities.task import TaskStatus
from mt_library.controllers.tags_controller import TagsController
from mt_library.controllers.events_controller import EventController
from mt_library.controllers.tasks_controller import TasksController
from mt_library.controllers.reg_tasks_controller import RegTaskController
from tests.test_factory import (
    TagFactory,
    TaskFactory,
    RegTaskFactory,
    EventFactory
    )


class ControllersTest(unittest.TestCase):
    def setUp(self):
        self.database = ':memory:'
        self.user_id = 8

        self.tags_controller = TagsController(self.user_id, DbTag(self.database))
        self.tasks_controller = TasksController(self.user_id, DbTask(self.database))
        self.reg_tasks_controller = RegTaskController(self.user_id, DbRegTask(self.database))
        self.events_controller = EventController(self.user_id, DbEvent(self.database))

        self.tag = TagFactory()
        self.task = TaskFactory()
        self.reg_task = RegTaskFactory()
        self.event = EventFactory()

        Connector(self.database).create_tables()

    def tearDown(self):
        Connector(self.database).drop_tables()

    """TAGS CONTROLLER TEST"""

    def test_create_tag(self):
        before_tests_count = Tag.select().count()
        self.tags_controller.create(self.tag)
        after_tags_count = Tag.select().count()
        self.assertEqual(before_tests_count + 1, after_tags_count)

    def test_get_tag_by_id(self):
        tag_with_id = self.tags_controller.create(self.tag)
        tag_from_test_method = self.tags_controller.get_by_id(tag_with_id.id)
        self.assertEqual(tag_with_id.id, tag_from_test_method.id)
        self.assertEqual(tag_with_id.name, tag_from_test_method.name)
        self.assertEqual(tag_with_id.user_id, tag_from_test_method.user_id)

    def test_update_tag(self):
        tag_with_id = self.tags_controller.create(self.tag)
        tag_with_id.name = "Homework"
        self.tags_controller.update(tag_with_id)
        tag_from_test_method = self.tags_controller.get_by_id(tag_with_id.id)
        self.assertEqual(tag_with_id.name, tag_from_test_method.name)

    def test_delete_tag(self):
        tag_with_id = self.tags_controller.create(self.tag)
        self.tags_controller.delete(tag_with_id.id)
        self.assertEqual(self.tags_controller.get_by_id(tag_with_id.id), None)

    """TASK CONTROLLER TEST"""

    def test_create_task(self):
        before_task_count = Task.select().count()
        self.tasks_controller.create(self.task)
        after_task_count = Task.select().count()
        self.assertEqual(before_task_count + 1, after_task_count)

    def test_get_task_by_id(self):
        task_with_id = self.tasks_controller.create(self.task)
        task_from_test_method = self.tasks_controller.get_by_id(task_with_id.id)
        self.assertEqual(task_with_id.id, task_from_test_method.id)

    def test_update_task(self):
        task_with_id = self.tasks_controller.create(self.task)
        task_with_id.title = "Homework"
        self.tasks_controller.update(task_with_id)
        task_from_test_method = self.tasks_controller.get_by_id(task_with_id.id)
        self.assertEqual(task_with_id.title, task_from_test_method.title)

    def test_delete_task(self):
        task_wit_id = self.tasks_controller.create(self.task)
        self.tasks_controller.delete(task_wit_id.id)
        self.assertEqual(self.tasks_controller.get_by_id(task_wit_id.id), None)

    def test_set_task_as_new(self):
        self.task.status = TaskStatus.IN_WORK.value
        task_id = self.tasks_controller.create(self.task).id
        self.tasks_controller.set_status(task_id, TaskStatus.NEW.value)
        task_from_test_method = self.tasks_controller.get_by_id(task_id)
        self.assertEqual(task_from_test_method.status, TaskStatus.NEW.value)

    def test_set_task_as_in_work(self):
        self.task.status = TaskStatus.NEW.value
        task_id = self.tasks_controller.create(self.task).id
        self.tasks_controller.set_status(task_id, TaskStatus.IN_WORK.value)
        task_from_test_method = self.tasks_controller.get_by_id(task_id)
        self.assertEqual(task_from_test_method.status, TaskStatus.IN_WORK.value)

    def test_set_task_as_done(self):
        self.task.status = TaskStatus.IN_WORK.value
        task_id = self.tasks_controller.create(self.task).id
        self.tasks_controller.set_status(task_id, TaskStatus.DONE.value)
        task_from_test_method = self.tasks_controller.get_by_id(task_id)
        self.assertEqual(task_from_test_method.status, TaskStatus.DONE.value)

    def test_return_user_tasks(self):
        self.tasks_controller.create(self.task)
        self.tasks_controller.create(self.task)
        tasks = self.tasks_controller.user_tasks()
        self.assertEqual(len(tasks), 2)

    def test_create_sub_task(self):
        task_id = self.tasks_controller.create(self.task).id
        sub_task = self.tasks_controller.create_sub_task(task_id, self.task)
        self.assertEqual(sub_task.parent_id, task_id)

    def test_return_sub_tasks(self):
        self.task.parent_id = None
        task_id = self.tasks_controller.create(self.task).id
        self.tasks_controller.create_sub_task(task_id, self.task)
        self.tasks_controller.create_sub_task(task_id, self.task)
        sub_tasks = self.tasks_controller.sub_tasks(task_id)
        self.assertEqual(len(sub_tasks), 2)

    """def assigne_task_on_user"""

    def test_add_user_for_read(self):
        user_id = 4
        task_id = self.tasks_controller.create(self.task).id
        self.tasks_controller.add_user_for_read(user_id=user_id, task_id=task_id)
        self.assertEqual(UserReadTasks.select().where(
            UserReadTasks.task_id == task_id and UserReadTasks.user_id == user_id).count(), 1)

    def test_add_user_for_write(self):
        user_id = 4
        task_id = self.tasks_controller.create(self.task).id
        self.tasks_controller.add_user_for_write(user_id=user_id, task_id=task_id)
        self.assertEqual(UserWriteTasks.select().where(
            UserWriteTasks.task_id == task_id and UserWriteTasks.user_id == user_id).count(), 1)

    def remove_user_for_read(self):
        user_id = 4
        task_id = self.tasks_controller.create(self.task).id
        self.tasks_controller.add_user_for_read(user_id=user_id, task_id=task_id)
        self.tasks_controller.remove_user_for_read(user_id=user_id, task_id=task_id)
        self.assertEqual(UserReadTasks.select().where(
            UserReadTasks.task_id == task_id and UserReadTasks.user_id == user_id).count(), 0)

    def remove_user_for_write(self):
        user_id = 4
        task_id = self.tasks_controller.create(self.task).id
        self.tasks_controller.add_user_for_write(user_id=user_id, task_id=task_id)
        self.tasks_controller.remove_user_for_write(user_id=user_id, task_id=task_id)
        self.assertEqual(UserWriteTasks.select().where(
            UserWriteTasks.task_id == task_id and UserWriteTasks.user_id == user_id).count(), 0)

    """REG TASK CONTROLLER TEST"""

    def test_create_reg_task(self):
        before_regs_count = RegTask.select().count()
        self.reg_tasks_controller.create(self.reg_task)
        after_regs_count = RegTask.select().count()
        self.assertEqual(before_regs_count + 1, after_regs_count)

    def test_update_reg_task(self):
        new_interval = 400
        new_datetime = datetime.datetime.now()
        reg_task_with_id = self.reg_tasks_controller.create(self.reg_task)
        reg_task_with_id.interval = new_interval
        reg_task_with_id.updating_date = new_datetime
        self.reg_tasks_controller.update(reg_task_with_id)
        reg_task_from_db = RegTask.get(RegTask.id == reg_task_with_id.id)
        self.assertEqual(reg_task_from_db.interval, new_interval)
        self.assertEqual(reg_task_from_db.updating_date, new_datetime)

    def test_delete_reg_task_by_id(self):
        reg_task_id = self.reg_tasks_controller.create(self.reg_task).id
        self.reg_tasks_controller.delete(reg_task_id)
        self.assertEqual(RegTask.select().where(RegTask.id == reg_task_id).count(), 0)

    def test_return_user_reg_tasks(self):
        self.reg_task.user_id = 8
        regs_count = 3
        for i in range(regs_count):
            self.reg_tasks_controller.create(self.reg_task)
        reg_tasks = self.reg_tasks_controller.user_reg_tasks()
        self.assertEqual(len(reg_tasks), regs_count)

    def test_create_task_with_reg(self):
        user_id = 8
        regular_task = TaskFactory()
        regular_task.user_id = user_id
        task_id = self.tasks_controller.create(regular_task).id
        before_tasks_count = len(self.tasks_controller.user_tasks())
        interval = 300
        big_interval = interval * 10
        updating_date = datetime.datetime.now() - datetime.timedelta(seconds=interval + 5)
        repeated_reg_task = RegTask(
            user_id=user_id,
            task_id=task_id,
            interval=interval,
            updating_date=updating_date)
        repeated_reg_task_big_interval = RegTask(
            user_id=user_id,
            task_id=task_id,
            interval=big_interval,
            updating_date=updating_date)
        self.reg_tasks_controller.create(repeated_reg_task)
        self.reg_tasks_controller.create(repeated_reg_task_big_interval)
        self.reg_tasks_controller.create_task_with_reg(self.tasks_controller)
        self.assertEqual(len(self.tasks_controller.user_tasks()), before_tasks_count + 1)

    """EVENT CONTROLLER TESTS"""

    def test_create_event(self):
        before_event_count = Event.select().count()
        self.events_controller.create(self.event)
        after_event_count = Event.select().count()
        self.assertEqual(before_event_count + 1, after_event_count)

    def test_update_event(self):
        place = "Some place"
        event_with_id = self.events_controller.create(self.event)
        event_with_id.place = place
        self.events_controller.update(event_with_id)
        event_from_db = Event.get(Event.id == event_with_id.id)
        self.assertEqual(event_from_db.place, place)

    def test_delete_event_by_id(self):
        event_id = self.events_controller.create(self.event).id
        self.events_controller.delete(event_id)
        self.assertEqual(Event.select().where(Event.id == event_id).count(), 0)

    def test_get_event_by_id(self):
        event_with_id = self.events_controller.create(self.event)
        event_from_test_method = self.events_controller.get_by_id(event_with_id.id)
        self.assertEqual(event_with_id.id, event_from_test_method.id)

    def test_return_user_events(self):
        self.events_controller.create(self.event)
        self.events_controller.create(self.event)
        events = self.events_controller.user_events()
        self.assertEqual(len(events), 2)

    def test_create_task_with_event(self):
        user_id = 8
        task_id = self.tasks_controller.create(self.task).id
        place = "Some place"
        event = EventFactory()
        event.user_id = user_id
        event.task_id = task_id
        event.place = place
        self.events_controller.create(event)
        before_event_count = len(self.events_controller.user_events())
        event_task = Event(
            user_id=user_id,
            task_id=task_id,
            place=place)
        self.events_controller.create(event_task)
        self.events_controller.create_task_with_event(self.tasks_controller)
        self.assertEqual(len(self.events_controller.user_events()), before_event_count + 1)
