"""Module provides all methods for working with Main Track """

from mt_library.entities.task import TaskStatus
from mt_library.entities.validator import validate_task, validate_reg_task
from mt_library.exceptions.exceptions import UserHasNoRightError, TaskDoesNotExistError
import mt_library.logger as log


def add_task(tasks_controller, task):
    validate_task(task)
    log.get_logger().info('Added task')
    return tasks_controller.create(task)


def add_reg_task(reg_task_controller, reg):
    validate_reg_task(reg)
    log.get_logger().info('Added regular task')
    reg_task_controller.create(reg)


def get_reg_tasks(reg_task_controller):
    """User's regular tasks"""

    return reg_task_controller.user_reg_tasks()


def get_reg_task_by_id(reg_task_controller, reg_id):
    return reg_task_controller.get_by_id(reg_id)


def get_tasks_created_by_reg_task(task_controller, reg_id):
    return task_controller.created_by_reg_task(reg_id)


def update_task(task_controller, task):
    validate_task(task)
    if user_can_write_task(task_controller, task.id):
        task_controller.update(task)
        log.get_logger().info('Updated task')
    else:
        log.get_logger().error('User has no rights for updating task')
        raise UserHasNoRightError


def update_reg_task(reg_task_controller, reg):
    validate_reg_task(reg)
    reg_task_controller.update(reg)
    log.get_logger().info('Updated reg task')


def get_task_by_id(task_controller, task_id):
    if user_can_read_task(task_controller, task_id):
        return task_controller.get_by_id(task_id)
    else:
        return None


def delete_task(task_controller, task_id):
    if user_can_write_task(task_controller, task_id):
        task_controller.delete(task_id)
        log.get_logger().info('Deleted task. id {}'.format(task_id))
    else:
        log.get_logger().error('User has no right for deleting task')
        raise UserHasNoRightError


def delete_reg_task(reg_task_controller, reg_task_id):
    reg_task_controller.delete(reg_task_id)
    log.get_logger().info('Deleted regular task. id {}'.format(reg_task_id))


def create_sub_task(task_controller, parent_id, task):
    parent_task = get_task_by_id(task_controller, parent_id)
    if parent_task is None:
        log.get_logger().error('Task does not exist')
        raise TaskDoesNotExistError
    validate_task(task)
    if user_can_write_task(task_controller, parent_id):
        task_controller.create_sub_task(parent_id, task)
        log.get_logger().info('Created sub task for task with id {}'.format(task.id))
    else:
        log.get_logger().error('User has no right for creating sub task')
        raise UserHasNoRightError


def filter_tasks(task_controller, *args):
    """Selects tasks according to the filter that you pass"""

    return task_controller.filter(args)


def get_sub_tasks(task_controller, task_id, recursive=False):
    if user_can_read_task(task_controller, task_id):
        return task_controller.sub_tasks(task_id, recursive)
    else:
        log.get_logger().error('User has no right for getting sub task')
    raise UserHasNoRightError


def get_parent_task(task_controller, task_id):
    """Return the task for which the given task is a subtask"""

    task = get_task_by_id(task_controller, task_id)
    if task is not None and task.parent_id is not None:
        return get_task_by_id(task_controller, task.parent_id)


def assign_task_on_user(task_controller, task_id, user_id):
    """Set a task for the user"""

    if user_can_write_task(task_controller, task_id):
        task_controller.assign_task_on_user(task_id, user_id)
        log.get_logger().info('Assigned task with ID: {} on user with ID: {}'.format(task_id, user_id))
    else:
        log.get_logger().error('User has no right for assigning this task')
        raise UserHasNoRightError


def add_user_for_read(task_controller, user_id, task_id):
    """Allow another user to read my task"""

    if user_can_write_task(task_controller, task_id):
        task_controller.add_user_for_read(user_id, task_id)
        log.get_logger().info('Gave user id: {} read access task id: {}'.format(user_id, task_id))
    else:
        log.get_logger().error('User has no right for giving access for this task')
        raise UserHasNoRightError


def add_user_for_write(task_controller, user_id, task_id):
    """Allow another user to read and write my task"""

    if user_can_write_task(task_controller, task_id):
        task_controller.add_user_for_write(user_id, task_id)
        log.get_logger().info('Gave user id: {} read and write access task id: {}'.format(user_id, task_id))
    else:
        log.get_logger().error('User has no right for giving access for this task')
        raise UserHasNoRightError


def remove_user_for_read(task_controller, user_id, task_id):
    """Prevent another user from reading my task"""

    if user_can_write_task(task_controller, task_id):
        task_controller.remove_user_for_read(user_id, task_id)
        log.get_logger().info('Remove from user id: {} read access to task id: {}'.format(user_id, task_id))
    else:
        log.get_logger().error('User has no right for removing access for this task')
        raise UserHasNoRightError


def remove_user_for_write(task_controller, user_id, task_id):
    """Prevent another user from reading and writing my task"""

    if user_can_write_task(task_controller, task_id):
        task_controller.remove_user_for_write(user_id, task_id)
        log.get_logger().info('Remove from user id: {} read access to task id: {}'.format(user_id, task_id))
    else:
        log.get_logger().error('User has no right for removing access for this task')
        raise UserHasNoRightError


def remove_all_users_for_read(task_controller, task_id):
    """Prevent all users from reading my task"""

    if user_can_write_task(task_controller, task_id):
        task_controller.remove_all_users_for_read(task_id)
        log.get_logger().info('Removed read access to task id: {} for all users'.format(task_id))
    else:
        log.get_logger().error('User has no right for removing access for this task')
        raise UserHasNoRightError


def remove_all_users_for_write(task_controller, task_id):
    """Prevent all users from reading and writing my task"""

    if user_can_write_task(task_controller, task_id):
        task_controller.remove_all_users_for_write(task_id)
        log.get_logger().info('Removed read and write access to task id: {} for all users'.format(task_id))
    else:
        log.get_logger().error('User has no right for removing access for this task')
        raise UserHasNoRightError


def get_users_can_read_task(task_controller, task_id):
    return task_controller.get_users_can_read(task_id)


def get_users_can_write_task(task_controller, task_id):
    return task_controller.get_users_can_write(task_id)


def user_tasks(task_controller):
    """Selects tasks that the user created"""

    return task_controller.user_tasks()


def assigned_tasks(tasks_controller):
    """Selects tasks that the user must perform"""

    return tasks_controller.assigned()


def can_read_tasks(task_controller):
    """Selects tasks that the user can view """

    return task_controller.can_read()


def can_write_tasks(task_controller):
    """Selects tasks that the user can view and modify"""

    return task_controller.can_write()


def tasks_with_status(task_controller, status):
    """Selects user tasks with the appropriate status"""

    return task_controller.with_status(status)


def set_task_status(task_controller, task_id, status):
    if user_can_write_task(task_controller, task_id):
        task_controller.set_status(task_id, status)
        log.get_logger().info('Set status {} to task id {}'.format(TaskStatus(status).name, task_id))
    else:
        log.get_logger().error('User has no right for changing status for this task')
        raise UserHasNoRightError


def add_tag(tags_controller, tag):
    tags_controller.create(tag)
    log.get_logger().info('Tag {} added'.format(tag.name))


def user_tags(tags_controller):
    """Selects tags that the user created"""

    return tags_controller.user_tags()


def update_tag(tags_controller, tag):
    if tags_controller.get_by_id(tag.id).user_id == tags_controller.user_id:
        tags_controller.update(tag)
    else:
        log.get_logger().error('User has no right')
        raise UserHasNoRightError


def get_tag_by_id(tags_controller, tag_id):
    return tags_controller.get_by_id(tag_id)


def delete_tag(tags_controller, tag_id):
    tag = tags_controller.get_by_id(tag_id)
    if tag.user_id == tags_controller.user_id:
        tags_controller.delete(tag_id)
        log.get_logger().info('Deleted tag')


def user_can_read_task(task_controller, task_id):
    task = task_controller.get_by_id(task_id)
    if task is not None:
        return (task.user_id == task_controller.user_id or
                task_controller.users_can_read(task_id) or
                task.assigned_user_id == task_controller.user_id or
                task_controller.users_can_write(task_id))


def user_can_write_task(task_controller, task_id):
    print('task_id:{0}'.format(task_id))
    task = task_controller.get_by_id(task_id)
    print('user_can_write_task. task.user_id: {0}, task is  None: {1}'.format(task.user_id, task is None))
    if task is not None:
        return (task.user_id == task_controller.user_id or
                task.assigned_user_id == task_controller.user_id or
                task_controller.users_can_write(task_id))


def add_event(events_controller, event):
    events_controller.create(event)
    log.get_logger().info('Added event')


def update_event(events_controller, event):
    events_controller.update(event)
    log.get_logger().info('Updated event')


def delete_event(events_controller, event_id):
    events_controller.delete(event_id)
    log.get_logger().info('Deleted event id {}'.format(event_id))


def get_event_by_id(events_controller, event_id):
    return events_controller.get_by_id(event_id)


def user_events(events_controller):
    """Selects events that the user created"""

    return events_controller.user_events()
