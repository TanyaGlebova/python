"""
The MainTrack project - simple planner of your affairs

All public methods for working with task manager are located in mt_library.commands module.

Entities (mt_library.entities package):

1) Task
2) Regular task
3) Event
4) Tag

Controllers (mt_library.controllers package):

1) TasksController
2) RegTaskController
3) EventController
4) TagsController

Controllers are used for connecting entities with database.
To work with public methods, at least you should pass
appropriate controller for entities with which you want to work.

Examples of using:

1) Task creating
>>> from mt_library.entities.task import Task
>>> from mt_library.controllers.tasks_controller import create_task_controller
>>> from mt_library.commands import create_sub_task
>>> task = Task(user_id=USER_ID, title="Some title", note="Some note")
>>> tasks_controller = create_task_controller(USER_ID, '/your_database_path')
>>> create_sub_task(tasks_controller, PARENT_ID, sub_task)

2) Subtask creating
>>> from mt_library.controllers.tasks_controller import create_task_controller
>>> from mt_library.entities.task import Task
>>> from mt_library.commands import create_sub_task
>>> sub_task = Task(user_id=USER_ID, title="Sub task", note="Some note")
>>> tasks_controller = create_task_controller(USER_ID, '/your_database_path')
>>> create_sub_task(tasks_controller, PARENT_ID, sub_task)

3) View user tasks
>>> from mt_library.controllers.tasks_controller import create_task_controller
>>> from mt_library.commands import user_tasks
>>> tasks_controller = create_task_controller(USER_ID, '/your_database_path')
>>> tasks = user_tasks(tasks_controller)
"""
