class Error(Exception):
    """Base for all exceptions"""
    pass


class InvalidTaskTimeError(Error):
    def __init__(self, begin, end):
        super().__init__('Begin time {} later than end time {}'.format(begin, end))


class InvalidRegTaskIntervalError(Error):
    def __init__(self, interval):
        super().__init__('Interval ( {} ) should be more than 5 minutes'.format(interval))


class TaskDoesNotExistError(Error):
    def __init__(self):
        super().__init__('''Task doesn't exist''')


class UserHasNoRightError(Error):
    def __init__(self):
        super().__init__('User has no right for this action')
