import datetime
from peewee import DoesNotExist
from mt_library.database.db_entities import RegTask, Connector
from mt_library.entities.reg_task import RegTask as RegTaskObject
from mt_library.entities.task import TaskStatus


class DbRegTask(Connector):

    @staticmethod
    def to_reg_task_instance(reg_task):
        return RegTaskObject(
            id=reg_task.id,
            user_id=reg_task.user_id,
            task_id=reg_task.task_id,
            interval=reg_task.interval,
            updating_date=reg_task.updating_date)

    def create(self, reg_task):
        return self.to_reg_task_instance(
            RegTask.create(
                id=reg_task.id,
                user_id=reg_task.user_id,
                task_id=reg_task.task_id,
                interval=reg_task.interval,
                updating_date=reg_task.updating_date))

    @staticmethod
    def update(reg_task):
        RegTask.update(
            interval=reg_task.interval,
            updating_date=reg_task.updating_date).where(RegTask.id == reg_task.id).execute()

    @staticmethod
    def delete_by_id(reg_id):
        RegTask.delete().where(RegTask.id == reg_id).execute()

    def get_by_id(self, reg_task_id):
        try:
            return self.to_reg_task_instance(
                 RegTask.get(RegTask.id == reg_task_id))
        except DoesNotExist:
            return None

    def user_reg_tasks(self, user_id):
        """Selects regular tasks that the user created"""

        return list(map(self.to_reg_task_instance, list(RegTask.select().where(RegTask.user_id == user_id))))

    @staticmethod
    def create_task_with_reg(db_task):
        """Creates a regular task based task"""

        for reg in RegTask.select():
            if(reg.updating_date + datetime.timedelta(seconds=reg.interval)) < datetime.datetime.now():
                try:
                    task = db_task.get_by_id(reg.task_id)
                    task.id = None
                    task.status = TaskStatus.NEW.value
                    task.reg_id = reg.id
                    db_task.create(task)
                    updating_date = (reg.updating_date + datetime.timedelta(seconds=reg.interval))
                    while (updating_date + datetime.timedelta(seconds=reg.interval)) < datetime.datetime.now():
                        updating_date += datetime.timedelta(seconds=reg.interval)
                    reg.updating_date = updating_date
                    reg.save()
                except DoesNotExist:
                    pass
