from peewee import DoesNotExist
from mt_library.database.db_entities import Event, Connector
from mt_library.entities.event import Event as EventObject
from mt_library.entities.task import TaskStatus


class DbEvent(Connector):

    @staticmethod
    def to_event_instance(event):
        return EventObject(
            id=event.id,
            user_id=event.user_id,
            task_id=event.task_id,
            place=event.place)

    def create(self, event):
        return self.to_event_instance(
            Event.create(
                id=event.id,
                user_id=event.user_id,
                task_id=event.task_id,
                place=event.place))

    @staticmethod
    def update(event):
        Event.update(
            place=event.place).where(Event.id == event.id).execute()

    @staticmethod
    def delete_by_id(event_id):
        Event.delete().where(Event.id == event_id).execute()

    def get_by_id(self, event_id):
        try:
            return self.to_event_instance(
                Event.get(Event.id == event_id))
        except DoesNotExist:
            return None

    def user_events(self, user_id):
        """Selects events that the user created"""

        return list(map(self.to_event_instance, list(Event.select().where(Event.user_id == user_id))))

    @staticmethod
    def create_task_with_event(db_task):
        """Creates event based task"""

        for event in Event.select():
            try:
                task = db_task.get_by_id(event.task_id)
                task.id = None
                task.status = TaskStatus.NEW.value
                task.event_id = event.id
                db_task.create(task)
                event.save()
            except DoesNotExist:
                pass
