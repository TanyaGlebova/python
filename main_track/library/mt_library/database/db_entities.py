import datetime

from peewee import (
    Proxy,
    Model,
    PrimaryKeyField,
    IntegerField,
    CharField,
    ForeignKeyField,
    DateTimeField,
    SqliteDatabase
    )
from mt_library.entities.task import TaskStatus, TaskPriority
db_proxy = Proxy()


class BaseModel(Model):
    """Base model for peewee """

    class Meta:
        database = db_proxy


class Tag(BaseModel):

    name = CharField()
    id = PrimaryKeyField(null=False)
    user_id = IntegerField(null=True)


class Task(BaseModel):

    title = CharField()
    note = CharField(default="")
    id = PrimaryKeyField(null=False)
    parent_id = IntegerField(null=True)
    user_id = IntegerField(null=True)
    assigned_user_id = IntegerField(null=True)
    begin = DateTimeField(null=True)
    end = DateTimeField(null=True)
    creating_date = DateTimeField(default=datetime.datetime.now)
    updating_date = DateTimeField(default=datetime.datetime.now)
    status = IntegerField(default=TaskStatus.NEW.value)
    tag_id = ForeignKeyField(Tag, backref='tasks', null=True)
    priority = IntegerField(default=TaskPriority.NO.value)
    reg_id = IntegerField(null=True)
    event_id = IntegerField(null=True)

    def save(self, *args, **kwargs):
        self.updating_date = datetime.datetime.now()
        return super(Task, self).save(*args, **kwargs)


class RegTask(BaseModel):

    id = PrimaryKeyField(null=False)
    user_id = IntegerField(null=True)
    task = ForeignKeyField(Task, null=True)
    interval = IntegerField()
    updating_date = DateTimeField()


class Event(BaseModel):

    id = PrimaryKeyField(null=False)
    user_id = IntegerField(null=True)
    task = ForeignKeyField(Task, null=True)
    place = CharField(default="")


class UserWriteTasks(BaseModel):
    """The relationship between user and task
       allowing to view and change the task"""

    user_id = IntegerField(null=True)
    task = ForeignKeyField(Task)


class UserReadTasks(BaseModel):
    """The relationship between user and task
       allowing to view the task"""

    user_id = IntegerField(null=True)
    task = ForeignKeyField(Task)


class Connector:
    """Establishing connection with database,
       creating and dropping tables"""

    def __init__(self, db_name='./main_track.db'):
        self.db_name = db_name
        self.database = SqliteDatabase(db_name)
        self.connected = False
        db_proxy.initialize(self.database)
        self.create_tables()

    def create_tables(self):
        if not self.connected:
            self.database.connect()
            self.connected = True
        Task.create_table(True)
        RegTask.create_table(True)
        Event.create_table(True)
        Tag.create_table(True)
        UserWriteTasks.create_table(True)
        UserReadTasks.create_table(True)

    def drop_tables(self):
        self.database.drop_tables(
            [Task, RegTask, Tag, Event, UserWriteTasks, UserReadTasks])
        self.database.close()
        self.connected = False
