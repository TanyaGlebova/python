import datetime
from peewee import DoesNotExist
from collections import deque
from mt_library.database.db_entities import (
    Task,
    RegTask,
    Event,
    UserWriteTasks,
    UserReadTasks,
    Connector
    )
from mt_library.entities.task import Task as TaskObject


class DbTask(Connector):

    @staticmethod
    def to_task_instance(task):
        return TaskObject(
            title=task.title,
            note=task.note,
            id=task.id,
            parent_id=task.parent_id,
            user_id=task.user_id,
            assigned_user_id=task.assigned_user_id,
            begin=task.begin,
            end=task.end,
            creating_date=task.creating_date,
            updating_date=task.updating_date,
            status=task.status,
            tag_id=task.tag_id,
            priority=task.priority,
            reg_id=task.reg_id,
            event_id=task.event_id)

    def create(self, task):
        return self.to_task_instance(
            Task.create(
                title=task.title,
                note=task.note,
                id=task.id,
                parent_id=task.parent_id,
                user_id=task.user_id,
                assigned_user_id=task.assigned_user_id,
                begin=task.begin,
                end=task.end,
                status=task.status,
                tag_id=task.tag_id,
                priority=task.priority,
                reg_id=task.reg_id,
                event_id=task.event_id))

    @staticmethod
    def update(task):
        Task.update(
            title=task.title,
            note=task.note,
            parent_id=task.parent_id,
            assigned_user_id=task.assigned_user_id,
            begin=task.begin,
            end=task.end,
            updating_date=datetime.datetime.now(),
            status=task.status,
            tag_id=task.tag_id,
            priority=task.priority,
        ).where(Task.id == task.id).execute()

    def get_by_id(self, task_id):
        try:
            return self.to_task_instance(Task.get(Task.id == task_id))
        except DoesNotExist:
            return None

    def with_status(self, user_id, status):
        """Selects user tasks with the appropriate status"""

        return list(map(self.to_task_instance, list(Task.select().where(Task.user_id == user_id,
                                                                        Task.status == status))))

    def user_task(self, user_id):
        """Selects tasks that the user created"""

        return list(map(self.to_task_instance, list(Task.select().where(Task.user_id == user_id))))

    def assigned(self, user_id):
        """Selects tasks that the user must perform"""

        return list(map(self.to_task_instance, list(
            Task.select().where(Task.assigned_user_id == user_id))))

    def can_read(self, user_id):
        """Selects tasks that the user can view """

        return list(map(self.to_task_instance, list(Task.select().join(UserReadTasks).where(
            UserReadTasks.task_id == Task.id, UserReadTasks.user_id == user_id))))

    def can_write(self, user_id):
        """Selects tasks that the user can view and modify"""

        return list(map(self.to_task_instance, list(Task.select().join(UserWriteTasks).where(
            UserWriteTasks.task_id == Task.id, UserWriteTasks.user_id == user_id))))

    def recursive_sub_tasks(self, queue, subs):
        """Returns all task subtasks recursively"""

        if not queue:
            return subs
        task = queue.popleft()
        for sub in self.sub_tasks(task.id):
            subs.append(sub)
            queue.append(sub)
        return self.recursive_sub_tasks(queue, subs)

    def sub_tasks(self, task_id, recursive=False):
        """Returns all task subtasks """

        if recursive:
            queue = deque()
            queue.append(self.get_by_id(task_id))
            subs = []
            return self.recursive_sub_tasks(queue, subs)
        return list(map(self.to_task_instance, list(Task.select().where(Task.parent_id == task_id))))

    @staticmethod
    def user_can_read(user_id, task_id):
        """Returns True or False depending on whether the user can view the task"""

        return UserReadTasks.select().where(UserReadTasks.task_id == task_id,
                                            UserReadTasks.user_id == user_id).count() == 1

    @staticmethod
    def user_can_write(user_id, task_id):
        """Returns True or False depending on whether the user can view and change the task"""

        return UserWriteTasks.select().where(UserWriteTasks.task_id == task_id,
                                             UserWriteTasks.user_id == user_id).count() == 1

    @staticmethod
    def add_user_for_read(user_id, task_id):
        """Create a user-task link for viewing task"""

        if UserReadTasks.select().where(UserReadTasks.task_id == task_id,
                                        UserReadTasks.user_id == user_id).count() == 0:
            UserReadTasks.create(user_id=user_id, task_id=task_id)

    @staticmethod
    def add_user_for_write(user_id, task_id):
        """Create a user-task link for viewing and modification of task"""

        if UserWriteTasks.select().where(UserWriteTasks.task_id == task_id,
                                         UserWriteTasks.user_id == user_id).count() == 0:
            UserWriteTasks.create(user_id=user_id, task_id=task_id)

    @staticmethod
    def get_users_can_read(task_id):
        users = list(UserReadTasks.select(UserReadTasks.user_id).where(UserReadTasks.task_id == task_id))
        return [usr.user_id for usr in users]

    @staticmethod
    def get_users_can_write(task_id):
        users = list(UserWriteTasks.select(UserWriteTasks.user_id).where(UserWriteTasks.task_id == task_id))
        return [usr.user_id for usr in users]

    @staticmethod
    def remove_user_for_read(user_id, task_id):
        """Delete a user-task link for viewing task"""

        UserReadTasks.delete().where(UserReadTasks.user_id == user_id, UserReadTasks.task_id == task_id).execute()

    @staticmethod
    def remove_user_for_write(user_id, task_id):
        """Delete a user-task link for viewing and modification of task"""

        UserWriteTasks.delete().where(UserWriteTasks.user_id == user_id, UserWriteTasks.task_id == task_id).execute()

    @staticmethod
    def remove_all_users_for_read(task_id):
        """Delete all user-task links for viewing task"""

        UserReadTasks.delete().where(UserReadTasks.task_id == task_id).execute()

    @staticmethod
    def remove_all_users_for_write(task_id):
        """Delete all user-task links for viewing and modification of task"""

        UserWriteTasks.delete().where(UserWriteTasks.task_id == task_id).execute()

    @staticmethod
    def delete_by_id(task_id):
        Task.delete().where(Task.id == task_id).execute()
        RegTask.delete().where(RegTask.task_id == task_id).execute()
        Event.delete().where(Event.task_id == task_id).execute()

    def filter(self, *args):
        """Selects tasks according to the filter that you pass"""

        return list(map(self.to_task_instance, list(Task.select().where(args))))

    def created_by_reg_task(self, user_id, reg_id):
        """Selects the tasks from which regular tasks are created"""

        return list(map(self.to_task_instance, list(Task.select().where(Task.user_id == user_id,
                                                                        Task.reg_id == reg_id))))

    def created_by_event(self, user_id, event_id):
        """Selects the tasks from which events are created"""

        return list(map(self.to_task_instance, list(Task.select().where(Task.user_id == user_id,
                                                                        Task.event_id == event_id))))
