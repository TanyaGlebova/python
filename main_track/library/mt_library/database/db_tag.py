from peewee import DoesNotExist
from mt_library.database.db_entities import Tag, Connector
from mt_library.entities.tag import Tag as TagObject


class DbTag(Connector):

    @staticmethod
    def to_tag_instance(tag):
        return TagObject(
            id=tag.id,
            name=tag.name,
            user_id=tag.user_id)

    def create(self, tag):
        return self.to_tag_instance(
            Tag.create(
                id=tag.id,
                name=tag.name,
                user_id=tag.user_id))

    @staticmethod
    def update(tag):
        Tag.update(
            name=tag.name).where(Tag.id == tag.id).execute()

    def get_by_id(self, tag_id):
        try:
            return self.to_tag_instance(Tag.get(Tag.id == tag_id))
        except DoesNotExist:
            return None

    def user_tags(self, user_id):
        """Selects tags that the user created"""

        return list(map(self.to_tag_instance, list(Tag.select().where(Tag.user_id == user_id))))

    @staticmethod
    def delete_by_id(tag_id):
        Tag.delete().where(Tag.id == tag_id).execute()
