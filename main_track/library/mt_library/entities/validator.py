from mt_library.exceptions.exceptions import InvalidRegTaskIntervalError, InvalidTaskTimeError

MIN_INTERVAL = 300  # minimum interval for repeating regular tasks


def validate_begin_time_later_than_end_time(begin, end):
    if begin and end:
        if begin > end:
            raise InvalidTaskTimeError(begin, end)


def validate_reg_task_interval(interval):
    if interval < MIN_INTERVAL:
        raise InvalidRegTaskIntervalError(interval)


def validate_task(task):
    validate_begin_time_later_than_end_time(task.begin, task.end)


def validate_reg_task(reg):
    validate_reg_task_interval(reg.interval)
