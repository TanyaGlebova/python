from enum import Enum


class TaskStatus(Enum):

    NEW = 0
    IN_WORK = 1
    DONE = 2

    @staticmethod
    def get_by_number(number):
        return TaskStatus(number)


class TaskPriority(Enum):

    NO = 0
    LOW = 1
    MEDIUM = 2
    HIGH = 3

    @staticmethod
    def get_by_number(number):
        return TaskPriority(number)


class Task:

    def __init__(
            self,
            title,
            note="",
            id=None,
            parent_id=None,
            user_id=None,
            assigned_user_id=None,
            begin=None,
            end=None,
            creating_date=None,
            updating_date=None,
            status=TaskStatus.NEW.value,
            tag_id=None,
            priority=TaskPriority.NO.value,
            reg_id=None,
            event_id=None):

        self.title = title
        self.note = note
        self.id = id
        self.parent_id = parent_id
        self.user_id = user_id
        self.assigned_user_id = assigned_user_id
        self.begin = begin
        self.end = end
        self.creating_date = creating_date
        self.updating_date = updating_date
        self.status = status
        self.tag_id = tag_id
        self.priority = priority
        self.reg_id = reg_id
        self.event_id = event_id
