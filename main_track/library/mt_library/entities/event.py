class Event:

    def __init__(
            self,
            user_id,
            task_id,
            place,
            id=None):

        self.id = id
        self.user_id = user_id
        self.task_id = task_id
        self.place = place
