class RegTask:

    def __init__(
            self,
            interval,
            user_id,
            task_id,
            updating_date,
            id=None):

        self.id = id
        self.user_id = user_id
        self.task_id = task_id
        self.interval = interval
        self.updating_date = updating_date
