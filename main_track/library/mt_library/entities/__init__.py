"""

    Modules of this directory contain the main project entities
    and the validator for checking some fields

    1) Event.py     - task-events
    2) Reg_task.py  - regular tasks
    3) Tag.py       - tag that defines the type of task and is used to group tasks
    4) Task.py      - basic model
    5) Validator.py - validation of dates

"""