class Tag:

    def __init__(
            self,
            name,
            id=None,
            user_id=None):

        self.name = name
        self.id = id
        self.user_id = user_id
