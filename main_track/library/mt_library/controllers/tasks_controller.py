from mt_library.database.db_task import DbTask
from mt_library.controllers.database_controller import DatabaseController


def create_task_controller(user_id, database_name):
    return TasksController(user_id, DbTask(database_name))


class TasksController(DatabaseController):
    def create(self, task):
        task.user_id = self.user_id
        return self.database.create(task)

    def update(self, task):
        self.database.update(task)

    def get_by_id(self, task_id):
        return self.database.get_by_id(task_id)

    def set_status(self, task_id, status):
        task = self.get_by_id(task_id)
        task.status = status
        self.update(task)

    def with_status(self, status):
        """Selects user tasks with the appropriate status"""

        return self.database.with_status(self.user_id, status)

    def user_tasks(self):
        """Selects tasks that the user created"""

        return self.database.user_task(self.user_id)

    def can_read(self):
        """Selects tasks that the user can view """

        return self.database.can_read(self.user_id)

    def can_write(self):
        """Selects tasks that the user can view and modify"""

        return self.database.can_write(self.user_id)

    def create_sub_task(self, parent_id, task):
        task.parent_id = parent_id
        return self.create(task)

    def sub_tasks(self, task_id, recursive=False):
        """Returns all task subtasks """

        return self.database.sub_tasks(task_id, recursive)

    def assign_task_on_user(self, task_id, user_id):
        task = self.get_by_id(task_id)
        task.assigned_user_id = user_id
        self.update(task)

    def assigned(self):
        """Selects tasks that the user must perform"""

        return self.database.assigned(self.user_id)

    def add_user_for_read(self, user_id, task_id):
        """Create a user-task link for viewing task"""

        self.database.add_user_for_read(user_id=user_id, task_id=task_id)

    def add_user_for_write(self, user_id, task_id):
        """Create a user-task link for viewing and modification of task"""

        self.database.add_user_for_write(user_id=user_id, task_id=task_id)

    def users_can_read(self, task_id):
        """Returns True or False depending on whether the user can view the task"""

        return self.database.user_can_read(task_id)

    def users_can_write(self, task_id):
        """Returns True or False depending on whether the user can view and change the task"""

        return self.database.user_can_write(task_id)

    def get_users_can_read(self, task_id):
        return self.database.get_users_can_read(task_id=task_id)

    def get_users_can_write(self, task_id):
        return self.database.get_users_can_write(task_id=task_id)

    def remove_user_for_read(self, user_id, task_id):
        """Delete a user-task link for viewing task"""

        return self.database.remove_user_for_read(user_id=user_id, task_id=task_id)

    def remove_user_for_write(self, user_id, task_id):
        """Delete a user-task link for viewing and modification of task"""

        return self.database.remove_user_for_write(user_id=user_id, task_id=task_id)

    def remove_all_user_for_read(self, task_id):
        """Delete all user-task links for viewing task"""

        return self.database.remove_all_users_for_read(task_id=task_id)

    def remove_all_user_for_write(self, task_id):
        """Delete all user-task links for viewing and modification of task"""

        return self.database.remove_all_users_for_write(task_id=task_id)

    def delete(self, task_id):
        self.database.delete_by_id(task_id)

    def filter(self, *args):
        """Selects tasks according to the filter that you pass"""

        return self.database.filter(args)

    def created_by_reg_task(self, reg_id):
        """Selects the tasks from which regular tasks are created"""

        return self.database.created_by_reg_task(self.user_id, reg_id)

    def created_by_event(self, event_id):
        """Selects the tasks from which events are created"""

        return self.database.created_by_event(self.user_id, event_id)
