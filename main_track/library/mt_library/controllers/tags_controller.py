from mt_library.database.db_tag import DbTag
from mt_library.controllers.database_controller import DatabaseController


def create_tags_controller(user_id, database_name):
    return TagsController(user_id, DbTag(database_name))


class TagsController(DatabaseController):
    def create(self, tag):
        tag.user_id = self.user_id
        return self.database.create(tag)

    def update(self, tag):
        return self.database.update(tag)

    def delete(self, tag_id):
        self.database.delete_by_id(tag_id)

    def get_by_id(self, tag_id):
        return self.database.get_by_id(tag_id)

    def user_tags(self):
        """Selects tags that the user created"""

        return self.database.user_tags(self.user_id)
