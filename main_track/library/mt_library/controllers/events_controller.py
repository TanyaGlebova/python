from mt_library.controllers.database_controller import DatabaseController
from mt_library.database.db_event import DbEvent


def create_events_controller(user_id, database_name):
    return EventController(user_id, DbEvent(database_name))


class EventController(DatabaseController):
    def create(self, event):
        return self.database.create(event)

    def update(self, event):
        self.database.update(event)

    def delete(self, event_id):
        self.database.delete_by_id(event_id)

    def get_by_id(self, event_id):
        return self.database.get_by_id(event_id)

    def user_events(self):
        """Selects events that the user created"""

        return self.database.user_events(self.user_id)

    def create_task_with_event(self, task_controller):
        """Creates event based task"""

        self.database.create_task_with_event(task_controller.database)
