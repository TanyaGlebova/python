from mt_library.controllers.database_controller import DatabaseController
from mt_library.database.db_reg_task import DbRegTask


def create_reg_tasks_controller(user_id, database_name):
    return RegTaskController(user_id, DbRegTask(database_name))


class RegTaskController(DatabaseController):
    def create(self, reg):
        return self.database.create(reg)

    def update(self, reg):
        self.database.update(reg)

    def delete(self, reg_id):
        self.database.delete_by_id(reg_id)

    def get_by_id(self, reg_id):
        return self.database.get_by_id(reg_id)

    def user_reg_tasks(self):
        """Selects regular tasks that the user created"""

        return self.database.user_reg_tasks(self.user_id)

    def create_task_with_reg(self, task_controller):
        """Creates a regular task based task"""

        self.database.create_task_with_reg(task_controller.database)

